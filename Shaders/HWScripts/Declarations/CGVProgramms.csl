; Shaders Script file
; Copyright (c) 2001-2003 Crytek Studios. All Rights Reserved.
; Author: Honich Andrey

Version (1.00)

//=========================================================================
// Input binding:
//
// ATTR0: Vertex
// ATTR1: 
// ATTR2: Normal
// ATTR3: Color
// ATTR4: Color1
// ATTR5: FOGCOORD
// ATTR8: TexCoord0
// ATTR9: TexCoord1
// ATTR10: TexCoord2
// ATTR11: TexCoord3
// ATTR12: TexCoord4
// ATTR13: TexCoord5
// ATTR14: Tangent
// ATTR15: Binormal
//=========================================================================


//=========================================================================
//Output registers:
//
// HPOS: Output vertex position
// COL0: Primary color.
// COL1: Secondary color.
// TEX0: Tex. coords 0
// TEX1: Tex. coords 1
// TEX2: Tex. coords 2
// TEX3: Tex. coords 3
//=========================================================================
////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Vertex Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGVProgramms.csl
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: Vertex shader common functions
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////



#include "CGVPMacro.csi"

Shader 'CGVProgramms'
(
  HW
  (
    DeclareCGScript 'PosCommon'
    {
      float4 vPos = IN.Position;
      OUT.HPosition = mul(ModelViewProj, vPos);
    }

    DeclareCGScript 'PosNULL'
    {
      float4 vPos = IN.Position;
    }

    DeclareCGScript 'PosBending'
    {
      float4 vPos = IN.Position;

      // Bend factor
      float fBF = max(vPos.z, 0) * Bend.z + Bend.w;
      fBF = fBF * fBF;
      fBF = fBF * fBF - Bend.w;

      float4 vP;
      vP.xy = Bend.xy * fBF;

      float fLength = length(vPos.xyz);

      vPos.xy = vPos.xy + vP.xy;
      float3 vDirect = normalize(vPos.xyz);
      vPos.xyz = vDirect * fLength;

      OUT.HPosition = mul(ModelViewProj, vPos);
    }

    DeclareCGScript 'PosBending_Merged'
    {
      float4 vPos = IN.Position;
      vPos.xyz = vPos.xyz - IN.Tex1.xyz;

      // Bend factor
      float fBF = max(vPos.z, 0) * IN.Color.z + vPos.w;
      fBF = fBF * fBF;
      fBF = fBF * fBF - vPos.w;

      float4 vP;
      vP.xy = IN.Color1.xy * fBF;

      float fLength = length(vPos.xyz);

      vPos.xy = vPos.xy + vP.xy;
      float3 vDirect = normalize(vPos.xyz);
      vPos.xyz = vDirect * fLength;

      OUT.HPosition = mul(ModelViewProj, vPos);
    }

    DeclareCGScript 'PosBending_Sprite'
    {
      float4 vPos = IN.Position - ObjPos;

      // Bend factor
      float fBF = max(vPos.z, 0) * Bend.z + Bend.w;
      fBF = fBF * fBF;
      fBF = fBF * fBF - Bend.w;

      float4 vP;
      vP.xy = Bend.xy * fBF;

      float fLength = length(vPos.xyz);

      vPos.xy = vPos.xy + vP.xy;
      float3 vDirect = normalize(vPos.xyz);
      vPos.xyz = vDirect * fLength;

      vPos = vPos + ObjPos;
      OUT.HPosition = mul(ModelViewProj, vPos);
    }

    DeclareCGScript 'PosBending_DetailObject'
    {
      float4 vPos = IN.Position;                         
                                    
      float4 Bend;
      Bend.w = IN.Position.w;
      Bend.z = 0.01; //IN.Color.w;
      sincos(Time.x+vPos.x*0.1+vPos.y*0.3, Bend.x, Bend.y);
                                                                                        
      // Bend factor                                     
      float fBF = max(vPos.z, 0) * Bend.z + Bend.w;      
      fBF = fBF * fBF;                                   
      fBF = fBF * fBF - Bend.w;                          
                                                          
      float4 vP;                                         
      vP.xy = Bend.xy * fBF;                             
                                                          
      float fLength = length(vPos.xyz);                  
                                                          
      vPos.xy = vPos.xy + vP.xy;                         
      float3 vDirect = normalize(vPos.xyz);              
      vPos.xyz = vDirect * fLength;
      
      OUT.HPosition = mul(ModelViewProj, vPos);
    }
    
    DeclareCGScript 'PosBeam'
    {
      float4 vPos = IN.Position;
      float3 vNormal = normalize(IN.Normal.xyz);
      float fiOrigLength = 1/GeomConstants.x;
      
      // fLerp = vrt->x/OrigLength
      float fLerp = vPos.x * fiOrigLength;
      
      // Pos.x = fLerp * Length
      vPos.x = fLerp * CameraPos.w;

      // fCurRadius = Lerp(StartRadius, EndRadius, fLerp)
      float fCurRadius = lerp(GeomConstants.z, GeomConstants.w, fLerp);
      
      float fiOrigWidth = 1/GeomConstants.y;
      vPos.yz = vPos.yz * fiOrigWidth * fCurRadius;

      OUT.HPosition = mul(ModelViewProj, vPos);
    }
    
    DeclareCGScript 'PosSilhouete'
    {
      float3 vNormal = normalize(IN.Normal.xyz);
      float4 vPos;
      vPos.xyz = IN.Position.xyz + vNormal.xyz * Constants.x;
      vPos.w = IN.Position.w;

      OUT.HPosition = mul(ModelViewProj, vPos);
    }
    
    DeclareCGScript 'PosTerrainOverlay'
    {
      float3 normal = IN.Normal.xyz;
      
      float4 vPos;
      vPos.xyz = IN.Position.xyz + normal * (0.05).xxx;
      vPos.w = IN.Position.w;
      float4 hPos = mul(ModelViewProj, vPos);
      hPos.w = hPos.w + 0.005;
      OUT.HPosition = hPos;                
    }

    DeclareCGScript 'PosTerrainLayerOverlay'
    {
      float3 normal = IN.Normal.xyz;
      
      float4 vPos;
      vPos.xyz = IN.Position.xyz + normal * (0.05).xxx;
      vPos.w = IN.Position.w;
      float4 hPos = mul(ModelViewProj, vPos);
      hPos.w = hPos.w + 0.005;
      OUT.HPosition = hPos;                
    }

    DeclareCGScript 'PosFur'
    {
      // compute final vert pos = (norm*offset + vert)
      float4 vPos;
      vPos.xyz = IN.Position.xyz + IN.TNormal.xyz * Modifiers.x;
      vPos.w = IN.Position.w;
      OUT.HPosition = mul(ModelViewProj, vPos);
    }
    
    DeclareCGScript 'PosWaterDeform'
    {
      float2 v = IN.Position.xy + NoisePos.xy;
      
      // 2D version of perlin noise
      float2 i = frac(v * 0.03125) * 32;   // index between 0 and B-1
      float2 f = frac(v);                  // fractional position

      // lookup in permutation table
      float2 p;
      p[0] = pg[ i[0]   ].w;
      p[1] = pg[ i[0]+1 ].w;
      p = p + i[1];

      // compute dot products between gradients and vectors
      float4 r;
      r[0] = dot( pg[ p[0] ].xy,   f);
      r[1] = dot( pg[ p[1] ].xy,   f - float2(1.0f, 0.0f) );
      r[2] = dot( pg[ p[0]+1 ].xy, f - float2(0.0f, 1.0f) );
      r[3] = dot( pg[ p[1]+1 ].xy, f - float2(1.0f, 1.0f) );

      // interpolate
      f = f*f*( float2(3.0f, 3.0f) - float2(2.0f, 2.0f)*f);
      r = lerp( r.xyyy, r.zwww, f[1] );
      float fNoise = lerp( r.x, r.y, f[0] );

      float4 vPos = IN.Position;
      vPos.z = vPos.z + fNoise * NoisePos.w;

      OUT.HPosition = mul(ModelViewProj, vPos);
    }

    DeclareCGScript 'PosWaterDeform_TNorm'
    {
      float2 v = IN.Position.xy + NoisePos.xy;
      
      // 2D version of perlin noise
      float2 i = frac(v * 0.03125) * 32;   // index between 0 and B-1
      float2 f = frac(v);                  // fractional position

      // lookup in permutation table
      float2 p;
      p[0] = pg[ i[0]   ].w;
      p[1] = pg[ i[0]+1 ].w;
      p = p + i[1];

      // compute dot products between gradients and vectors
      float4 r;
      r[0] = dot( pg[ p[0] ].xy,   f);
      r[1] = dot( pg[ p[1] ].xy,   f - float2(1.0f, 0.0f) );
      r[2] = dot( pg[ p[0]+1 ].xy, f - float2(0.0f, 1.0f) );
      r[3] = dot( pg[ p[1]+1 ].xy, f - float2(1.0f, 1.0f) );

      // interpolate
      f = f*f*( float2(3.0f, 3.0f) - float2(2.0f, 2.0f)*f);
      r = lerp( r.xyyy, r.zwww, f[1] );
      float fNoise = lerp( r.x, r.y, f[0] );

      float4 vPos = IN.Position;
      vPos.xyz = vPos.xyz + fNoise * NoisePos.w * IN.TNormal.xyz;

      OUT.HPosition = mul(ModelViewProj, vPos);
    }

    DeclareCGScript 'PosWaterDeform_PlusWaterLevel'
    {
      float2 v = IN.Position.xy + NoisePos.xy;
      
      // 2D version of perlin noise
      float2 i = frac(v * 0.03125) * 32;   // index between 0 and B-1
      float2 f = frac(v);                  // fractional position

      // lookup in permutation table
      float2 p;
      p[0] = pg[ i[0]   ].w;
      p[1] = pg[ i[0]+1 ].w;
      p = p + i[1];

      // compute dot products between gradients and vectors
      float4 r;
      r[0] = dot( pg[ p[0] ].xy,   f);
      r[1] = dot( pg[ p[1] ].xy,   f - float2(1.0f, 0.0f) );
      r[2] = dot( pg[ p[0]+1 ].xy, f - float2(0.0f, 1.0f) );
      r[3] = dot( pg[ p[1]+1 ].xy, f - float2(1.0f, 1.0f) );

      // interpolate
      f = f*f*( float2(3.0f, 3.0f) - float2(2.0f, 2.0f)*f);
      r = lerp( r.xyyy, r.zwww, f[1] );
      float fNoise = lerp( r.x, r.y, f[0] );

      float4 vPos = IN.Position;
      //float fDeltaZ = abs(WaterLevel.x - vPos.z);
      //fNoise = fNoise * clamp(fDeltaZ * WaterLevel.z, 0, vPos.w);
      //float fWaterAmplitude = (vPos.z < WaterLevel.x) ? 0 : NoisePos.w;
      vPos.z = WaterLevel.x + fNoise * NoisePos.w;

      OUT.HPosition = mul(ModelViewProj, vPos);
    }

    DeclareCGScript 'PosCommon_PlusWaterLevel'
    {
      float4 vPos = IN.Position;
      vPos.z = WaterLevel.x;

      OUT.HPosition = mul(ModelViewProj, vPos);
    }

    DeclareCGScript 'CommonSubroutines'
    {
      FLOAT4 EXPAND( FLOAT4 a )
      {
        FLOAT4 result;
        if (CGC)
          result = 2.h*(a - 0.5h);
        else
          result = a*2.h - 1.h;
        return result;
      }
      FLOAT3 EXPAND( FLOAT3 a )
      {
        FLOAT3 result;
        if (CGC)
          result = 2.h*(a - 0.5h);
        else
          result = a*2.h - 1.h;
        return result;
      }
      FLOAT2 EXPAND( FLOAT2 a )
      {
        FLOAT2 result;
        if (CGC)
          result = 2.h*(a - 0.5h);
        else
          result = a*2.h - 1.h;
        return result;
      }
      FLOAT EXPAND( FLOAT a )
      {
        FLOAT result;
        if (CGC)
          result = 2.h*(a - 0.5h);
        else
          result = a*2.h - 1.h;
        return result;
      }

      FLOAT Luminance( FLOAT3 vColor )
      {
	      return dot(vColor, FLOAT3 (0.30, 0.59f, 0.11f));
      }

      FLOAT3 HDREncode( FLOAT3 a )
      {
        FLOAT3 result;
# if defined(_HDR) && defined(_HDR_FAKE)
        result = a * HDR_OVERBRIGHT;
# else
        result = a * 2;
# endif          
        return result;
      }
      FLOAT3 HDREncodeLM( FLOAT3 a )
      {
        FLOAT3 result;
# if defined(_HDR) && defined(_HDR_FAKE)
        result = a * HDR_OVERBRIGHT * 2;
# else
        result = a * 4;
# endif          
        return result;
      }
      FLOAT3 HDREncodeAmb( FLOAT3 a )
      {
        FLOAT3 result;
# if defined(_HDR) && defined(_HDR_FAKE)
        result = a * HDR_OVERBRIGHT / 2;
# else
        result = a;
# endif          
        return result;
      }

      FLOAT3 HDRFogBlend( FLOAT3 vColor, FLOAT fFog, FLOAT3 vFogColor )
      {
        FLOAT3 result;
# if defined(_HDR) && defined(_HDR_FAKE)
        result = lerp(HDREncodeAmb(vFogColor), vColor, fFog);
# else
        result = vColor;
# endif        
        return result;
      }

      FLOAT4 HDRFogBlend( FLOAT4 vColor, FLOAT fFog, FLOAT3 vFogColor )
      {
        FLOAT4 result;
# if defined(_HDR) && defined(_HDR_FAKE)
        result.xyz = lerp(HDREncodeAmb(vFogColor), vColor, fFog);
        result.a = vColor.a;
# else
        result = vColor;
# endif        
        return result;
      }


# define LOG(a, b)   ( log(b) / log(a) )

# define EXP_BASE    (1.06)
# define EXP_OFFSET  (128.0)

    float4 EncodeHDR_RGB_RGBE8(in float3 rgb)
    {
	    // Compute a common exponent
	    float fLen = dot(rgb.rgb, 1.0);
	    float fExp = LOG(EXP_BASE, fLen);

	    float4 ret;
	    ret.a = (fExp + EXP_OFFSET) / 256;
	    ret.rgb = rgb / fLen;

	    return ret;
    }
    
    float3 DecodeHDR_RGBE8_RGB(in float4 rgbe)
    {
	    float fExp = rgbe.a * 256 - EXP_OFFSET;
	    float fScale = pow(EXP_BASE, fExp);

	    return (rgbe.rgb * fScale);
    }

# ifdef _PIX_OUT
      void HDROutput( out pixout OUT, FLOAT4 Color, FLOAT fDepth, FLOAT3 FogColor, FLOAT fFogFactor )
      {
        FLOAT3 result = HDRFogBlend(Color.xyz, fFogFactor, FogColor);
  # ifndef _MRT
        OUT.Color = float4(result, Color.a);
  # else
        //float4 rgbe = EncodeHDR_RGB_RGBE8(result.xyz);
        //OUT.Color0.xyz = rgbe.xyz;
        //OUT.Color0.a = Color.a;
        //OUT.Color1 = float4(rgbe.a, 0, 0, 1);
      	float lum = Luminance(result.xyz) / HDRFAKE_MAXOVERBRIGHT;
      	OUT.Color0 = float4(result.xyz, Color.a);
      	OUT.Color1 = float4(lum,0,0,1);
  # endif
  # ifdef _DEPTH_OUT
        OUT.Depth = fDepth;
  # endif
      }

      void HDROutput( out pixout OUT, FLOAT4 Color, FLOAT fDepth)
      {
  # ifndef _MRT
        OUT.Color = Color;
  # else
        //float4 rgbe = EncodeHDR_RGB_RGBE8(Color.xyz);
        //OUT.Color0.xyz = rgbe.xyz;
        //OUT.Color0.a = Color.a;
        //OUT.Color1 = float4(rgbe.a, 0, 0, 1);
      	float lum = Luminance(Color.xyz) / HDRFAKE_MAXOVERBRIGHT;
      	OUT.Color0 = Color;
      	OUT.Color1 = float4(lum,0,0,1);
  # endif
  # ifdef _DEPTH_OUT
        OUT.Depth = fDepth;
  # endif
      }
# endif

      FLOAT3 GetNormalMap( sampler2D bumpMap, float2 bumpTC )
      {
        FLOAT3 bumpNormal;
# ifdef _3DC
        bumpNormal.xy = EXPAND(tex2D(bumpMap, bumpTC.xy).xy);
        bumpNormal.z = sqrt(1 - dot(bumpNormal.xy, bumpNormal.xy));
# elif defined (_CxV8U8)
        bumpNormal.xyz = tex2D(bumpMap, bumpTC.xy).xyz;
# elif defined (_V8U8)
        bumpNormal.xy = tex2D(bumpMap, bumpTC.xy).xy;
        bumpNormal.z = sqrt(1 - dot(bumpNormal.xy, bumpNormal.xy));
# else        
        bumpNormal = EXPAND(tex2D(bumpMap, bumpTC.xy).xyz);
# endif
        return bumpNormal;
      }

      FLOAT DepthBias( FLOAT fHeight, FLOAT3 vNormal, FLOAT3 vView, FLOAT2 vZW )
      {
        FLOAT3 vN = vNormal * fHeight;
        FLOAT fOffs = dot(vView, vN) * 0.02;
        FLOAT fOrigZ = vZW.x;
        return (fOrigZ - fOffs) / vZW.y;
      }
      
    }
  )
)

