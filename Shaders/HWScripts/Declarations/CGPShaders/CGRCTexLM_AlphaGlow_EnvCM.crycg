////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCTexLM_AlphaGlow_EnvCM.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      MainInput { uniform sampler2D baseMap : texunit0,
                  uniform sampler2D lightMap : texunit1,
                  uniform samplerCUBE envMap : texunit2,
                  uniform float4 Ambient,
                  uniform float4 Reflect }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        float4 decalColor = tex2D(baseMap, IN.Tex0.xy);
        // load the lm
        float4 lmColor = tex2D(lightMap, IN.Tex1.xy);
        // load the environment map
        float4 envColor = texCUBE(envMap, IN.Tex2.xyz);

        float3 dif = (decalColor.xyz * lmColor.xyz) * 4;
        float3 amb = lerp(decalColor.xyz*Ambient.xyz, envColor.xyz, Reflect.x);
        amb = amb.xyz + decalColor.xyz * decalColor.a;

        // finally add them all together
        OUT.Color.xyz = amb + dif;
        OUT.Color.w = Ambient.w;
      }


