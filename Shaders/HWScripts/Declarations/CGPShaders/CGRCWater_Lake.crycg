////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCWater_Lake.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////



      #include "../CGVPMacro.csi"

      MainInput { uniform sampler2D bumpMap : texunit0,
                  uniform sampler2D envMap : texunit1,
                  uniform sampler2D fresnelMap : texunit2,
                  uniform sampler2D baseMap : texunit3,
                  uniform float4 Matrix,
                  uniform float4 ReflectAmount,
                  uniform float4 WaterColor }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_T3
          OUT_C0
          OUT_C1
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        float4 bumpColor = tex2D(bumpMap, IN.Tex0.xy);
# ifdef _PS_1_1
        float4 env = offsettex2D(envMap, IN.Tex1.xy, bumpColor, Matrix);
# else
        float2 newst = IN.Tex1.xy / IN.Tex1.w;
        newst = newst.xy + Matrix.xy * bumpColor.xx + Matrix.zw * bumpColor.yy;
        float4 env = tex2D(envMap, newst);
# endif        
        float4 fresnelColor = tex2D(fresnelMap, IN.Tex2.xy);
        float4 baseColor = tex2D(baseMap, IN.Tex3.xy);

        float3 color = lerp(HDREncodeAmb(baseColor.xyz), env.xyz, ReflectAmount.xyz);
        
        OUT.Color.xyz = HDRFogBlend(color.xyz, IN.Color1.w, GlobalFogColor.xyz);
        float fTransp = fresnelColor.b + baseColor.b-0.5;
        
        OUT.Color.a = IN.Color.a* ReflectAmount.a * fTransp;
      }
      
