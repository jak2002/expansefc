////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCHeat_Alpha.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


#include "../CGVPMacro.csi"

MainInput 
{
  uniform sampler2D baseMap : texunit0,
  uniform sampler2D bumpMap : texunit1,
  uniform samplerCUBE normCubeMap : texunit2,
  uniform float4 Heat 
}

DeclarationsScript
{
  struct vertout
  {
    OUT_T0
    OUT_T1
    OUT_T2
  };

  FOUT
}

CoreScript
{
  // load the decal
  float4 baseColor = tex2D(baseMap, IN.Tex0.xy);
  float4 bumpColor = 2*(tex2D(bumpMap, IN.Tex1.xy)-0.5);
  float4 lightVec  = 2*(texCUBE(normCubeMap, IN.Tex2.xyz)-0.5);
  float fLum= dot(baseColor.xyz, float3(0.33, 0.59, 0.11));

  float fDot= saturate(dot(bumpColor.xyz, lightVec.xyz));
  float fFresnel=1-fDot;
  fFresnel*=fFresnel;
  
  OUT.Color.xyz = 2*fLum*fFresnel*Heat.xyz;
  OUT.Color.a = 1;
}