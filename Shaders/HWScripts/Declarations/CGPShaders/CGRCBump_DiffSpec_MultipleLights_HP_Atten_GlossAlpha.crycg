////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCBump_DiffSpec_MultipleLights_HP_Atten_GlossAlpha.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      MainInput { uniform sampler2D baseMap : texunit0,
                  uniform sampler2D bumpMap : texunit1,
                  uniform float4 Diffuse,
	                uniform float4 Specular }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_C0
          OUT_C1
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        float4 decalColor = tex2D(baseMap, IN.Tex0.xy);
        // load the bump normal
        float4 bumpNormal = 2*(tex2D(bumpMap, IN.Tex1.xy)-0.5);

        // Light vector from input color
        float3 lVec = 2 * (IN.Color.xyz - 0.5);
        // Half vector from input color
        float3 hVec = 2 * (IN.Color1.xyz - 0.5);
        float NdotL = saturate(dot(lVec, bumpNormal.xyz));
        float NdotH = saturate(dot(hVec, bumpNormal.xyz));
        float3 dif = (decalColor.xyz * NdotL * IN.Color.w * Diffuse.xyz) * 2;
        float  specVal = saturate((NdotH - 0.75)*4);
        specVal = specVal * specVal * IN.Color.w * decalColor.w;
        float3 spec = (specVal * Specular.xyz)*2;

        // finally add them all together
        OUT.Color.xyz = dif + spec;
        OUT.Color.w = Diffuse.w;
      }

