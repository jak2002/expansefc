////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRC_HDR_Spr_FV_PS20.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////

#include "../CGVPMacro.csi"

NoFog
PS20Only

MainInput 
{
  uniform sampler2D baseMap : texunit0,
  uniform sampler2D fogMap : texunit1,
  uniform sampler2D fogEnterMap : texunit2,
  uniform float4 FogColor
}

DeclarationsScript
{
  struct vertout
  {
    OUT_T0
    OUT_T1
    OUT_T2
    OUT_C0
  };

  FOUT
}

CoreScript
{
  float4 vSample = tex2D(baseMap, IN.Tex0.xy) * IN.Color;
  // load the fog
  float4 fogColor = tex2D(fogMap, IN.Tex1.xy);
  float4 fogEnterColor = tex2D(fogEnterMap, IN.Tex2.xy);

  float fog = fogColor.a * fogEnterColor.a;
  vSample.xyz = HDREncode(vSample.xyz);
  vSample.xyz = HDREncodeAmb(FogColor.xyz)*fog + vSample.xyz*(1-fog);

  OUT.Color = vSample;
}

