////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCBump_DiffSpec_SingleLight_GlossAlpha_EnvLight.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      MainInput { uniform sampler2D baseMap : texunit0,
                  uniform sampler2D bumpMap : texunit1,
                  uniform samplerCUBE normCubeMapLV : texunit2,
                  uniform samplerCUBE normCubeMapHA : texunit3,
                  uniform float4 Diffuse,
                  uniform float4 Specular,
                  uniform float4 Ambient }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_T3
          OUT_C0
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        float4 decalColor = tex2D(baseMap, IN.Tex0.xy);
        // load the bump normal
        float4 bumpNormal = 2*(tex2D(bumpMap, IN.Tex1.xy)-0.5);

        // Light vector from normalization cube-map
        float4 lVec = 2*(texCUBE(normCubeMapLV, IN.Tex2.xyz)-0.5);
        // Half angle vector from normalization cube-map
        float4 hVec = 2*(texCUBE(normCubeMapHA, IN.Tex3.xyz)-0.5);
        
        float NdotL = saturate(dot(lVec.xyz, bumpNormal.xyz));
        float NdotH = saturate(dot(hVec.xyz, bumpNormal.xyz));
        float3 dif = (decalColor.xyz * NdotL * Diffuse.xyz) * 2;
        float  specVal = saturate((NdotH - 0.75)*4);
        specVal = specVal * specVal;
        float3 spec = (specVal * Specular.xyz * decalColor.w) * 2;
        float3 amb = IN.Color.xyz * decalColor.xyz;

        // finally add them all together
        OUT.Color.xyz = amb + dif + spec;
        OUT.Color.w = Ambient.w;
      }

