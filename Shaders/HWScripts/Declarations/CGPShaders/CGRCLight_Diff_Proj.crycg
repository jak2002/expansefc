////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCLight_Diff_Proj.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      MainInput { uniform sampler2D baseMap : texunit0,
                  uniform samplerCUBE normCubeMap : texunit1,
                  uniform samplerCUBE projMap : texunit2,
                  uniform float4 Diffuse }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_C0
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        float4 decalColor = tex2D(baseMap, IN.Tex0.xy);

        float4 projColor = texCUBE(projMap, IN.Tex2.xyz);

        // Light vector from input color
        float lVec = 2*(texCUBE(normCubeMap, IN.Tex1.xyz).b-0.5);
        float NdotL = lVec;
        float3 dif = (decalColor.xyz * NdotL * projColor.xyz * Diffuse.xyz * IN.Color.xyz) * 2;

        // finally add them all together
        OUT.Color.xyz = dif;
        OUT.Color.w = decalColor.w * Diffuse.w;
      }


