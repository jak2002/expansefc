
#include "../CGVPMacro.csi"

NoFog
PS20Only

MainInput 
{
  uniform sampler2D baseMapRG : texunit0,
  uniform sampler2D baseMapBA : texunit1,
  uniform sampler2D lumMap : texunit2,
  uniform FLOAT4 Params
}

DeclarationsScript
{
  struct vertout
  {
    OUT_T0
  };

  FOUT
}

CoreScript
{
  FLOAT fMiddleGray = Params.x;
  FLOAT fBrightOffset = Params.y;
  FLOAT fBrightThreshold = Params.z;
  
  FLOAT4 vSample;
  vSample.xy = tex2D(baseMapRG, IN.Tex0.xy).xy;
  vSample.zw = tex2D(baseMapBA, IN.Tex0.xy).xy;
  FLOAT  fAdaptedLum = tex2D(lumMap, float2(0.5f, 0.5f));
  
  // Determine what the pixel's value will be after tone-mapping occurs
  vSample.rgb *= fMiddleGray/(fAdaptedLum + 0.001);
  
  // Subtract out dark pixels
  vSample.rgb -= fBrightThreshold;
  
  // Clamp to 0
  vSample = max(vSample, 0.0);
  
  // Map the resulting value into the 0 to 1 range. Higher values for
  // BRIGHT_PASS_OFFSET will isolate lights from illuminated scene 
  // objects.
  vSample.rgb /= (fBrightOffset+vSample);

  OUT.Color = vSample;
}

