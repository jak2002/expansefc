////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCHeat_Alpha_MultipleLights_Atten.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      MainInput { uniform sampler2D baseMap : texunit0,
                  uniform sampler2D bumpMap : texunit1,
                  uniform sampler2D attenMap : texunit2,
                  uniform float4 Heat }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_C0
          OUT_C1
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        float4 decalColor = tex2D(baseMap, IN.Tex0.xy);
        // load the bump normal
        float4 bumpNormal = 2*(tex2D(bumpMap, IN.Tex1.xy)-0.5);
        float3 atten = saturate((2*(IN.Color.xyz-0.5)) * -(2*(IN.Color.xyz-0.5)) + (1-tex2D(attenMap, IN.Tex2.xy).xyz));

        // Light vector from input color
        float3 lVec = 2 * (IN.Color1.xyz - 0.5);
        float NdotL = saturate(dot(lVec, bumpNormal.xyz));
        float3 dif = (NdotL * Heat.a * atten.b) * 2;

        // finally add them all together
        OUT.Color.xyz = Heat.a; //dif;
        OUT.Color.w = decalColor.w;
      }

