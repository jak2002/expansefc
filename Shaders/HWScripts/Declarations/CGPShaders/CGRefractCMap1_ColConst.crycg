////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRefractCMap1_ColConst.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////



      #include "../CGVPMacro.csi"

      MainInput { uniform sampler2D bumpMap : register(s0),
                  uniform samplerCUBE envMap : register(s3),
                  uniform float4 RefrAmount,
                  uniform float4 Ambient }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_T3
          OUT_C0
          OUT_C1
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        float4 bumpColor = tex2D(bumpMap, IN.Tex0.xy);
# ifdef _PS_1_1
        float4 env = texCUBE_dp3x3(envMap, IN.Tex3.xyz, IN.Tex1, IN.Tex2, bumpColor);
# else
        float3 newst = float3(dot(IN.Tex1.xyz, bumpColor.xyz),
                              dot(IN.Tex2.xyz, bumpColor.xyz),
                              dot(IN.Tex3.xyz, bumpColor.xyz));
        float4 env = texCUBE(envMap, newst);
# endif                
        float3 vColor = HDREncodeAmb(env.xyz * IN.Color.xyz * RefrAmount.a);
        OUT.Color.xyz = HDRFogBlend(vColor.xyz, IN.Color1.w, GlobalFogColor.xyz);
        OUT.Color.a = IN.Color.a * Ambient.a;
      }

