////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCBump_DiffSpec_SingleLight_Proj_EnvLight_PS20.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      PS20Only

      MainInput { uniform sampler2D baseMap : texunit0,
                  uniform sampler2D bumpMap : texunit1,
                  uniform samplerCUBE projMap : texunit2,
                  uniform sampler2D attenMap : texunit3,
                  uniform float4 Ambient,
                  uniform float4 Diffuse,
                  uniform float4 Specular }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_T3
          OUT_T4
          OUT_T5
          OUT_C0
          OUT_C1
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        half4 decalColor = tex2D(baseMap, IN.Tex0.xy);
        // load the bump normal
        float4 bumpNormal = 2*(tex2D(bumpMap, IN.Tex1.xy)-0.5);
        // load the projector filter map
        half4 projColor = texCUBE(projMap, IN.Tex2.xyz);
        half atten = saturate((2*(IN.Color.b-0.5)) * -(2*(IN.Color.b-0.5)) + (1-tex2D(attenMap, IN.Tex3.xy).b));

        // normalize post-filtered bump normals
        bumpNormal.xyz = normalize(bumpNormal.xyz);

        // normalize light vector
        float3 lightVec = normalize(IN.Tex4.xyz);
        float fDif = saturate(dot(lightVec.xyz, bumpNormal.xyz));

        // normalize view vector
        float3 viewVec = normalize(IN.Tex5.xyz);
        float3 reflVec = (2*dot(lightVec.xyz, bumpNormal.xyz)*bumpNormal.xyz)-lightVec.xyz;
        float NdotR = saturate(dot(reflVec.xyz, viewVec.xyz));
        half fSpec = pow(NdotR, Specular.w);

        half3 dif = (decalColor.xyz * fDif * atten * Diffuse.xyz * projColor.xyz) * 2;
        half3 spec = (fSpec * atten * projColor.xyz * Specular.xyz) * 2;
        half3 amb = IN.Color1.xyz * decalColor.xyz;

        // finally add them all together
        OUT.Color.xyz = amb + dif + spec;
        OUT.Color.w = decalColor.w * Ambient.w;
      }

