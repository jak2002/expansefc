////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCBump_DiffSpec_MultipleLights_ProjAtten_GlossAlpha_PS20.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      PS20Only

      MainInput { uniform sampler2D baseMap : texunit0,
                  uniform sampler2D bumpMap : texunit1,
                  uniform samplerCUBE normCubeMapLV : texunit2,
                  uniform samplerCUBE normCubeMapHA : texunit3,
                  uniform samplerCUBE projMap : texunit4,
                  uniform sampler2D attenMap : texunit5,
                  uniform float4 Diffuse,
                  uniform float4 Specular }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_T3
          OUT_T4
          OUT_T5
          OUT_C0
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        half4 decalColor = tex2D(baseMap, IN.Tex0.xy);
        // load the bump normal
        float4 bumpNormal = tex2D(bumpMap, IN.Tex1.xy)*2-1;
        // load the projector filter map
        half4 projColor = texCUBE(projMap, IN.Tex4.xyz);
        half a = IN.Color.b*2-1;
        half atten = saturate(a * -a + (1-tex2D(attenMap, IN.Tex5.xy).b));

        // Light vector from normalization cube-map
        float4 lVec = texCUBE(normCubeMapLV, IN.Tex2.xyz)*2-1;
        // Half angle vector from normalization cube-map
        float4 hVec = texCUBE(normCubeMapHA, IN.Tex3.xyz)*2-1;
        
        float NdotL = saturate(dot(lVec.xyz, bumpNormal.xyz));
        float NdotH = saturate(dot(hVec.xyz, bumpNormal.xyz));
        float  specVal = pow(NdotH,8);
        half3 proj = atten * projColor.xyz;
        half3 dif = (decalColor.xyz * NdotL * Diffuse.xyz * proj.xyz) * 2;
        half3 spec = (specVal * decalColor.w * Specular.xyz * proj.xyz) * 2;

        // finally add them all together
        OUT.Color.xyz = dif + spec;
        OUT.Color.w = Diffuse.w;
      }

