////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCBump_DiffSpec_MultipleLights_ProjAtten_Gloss_PS20.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      PS20Only

      MainInput { uniform sampler2D baseMap : register(s0),
                  uniform sampler2D bumpMap : register(s1),
                  uniform samplerCUBE normCubeMapLV : register(s2),
                  uniform samplerCUBE normCubeMapHA : register(s3),
                  uniform samplerCUBE projMap : register(s4),
                  uniform sampler2D attenMap : register(s5),
                  uniform sampler2D glossMap : register(s6),
                  uniform float4 Diffuse,
                  uniform float4 Specular }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
          OUT_T3
          OUT_T4
          OUT_T5
          OUT_T6
          OUT_C0
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        half4 decalColor = tex2D(baseMap, IN.Tex0.xy);
        // load the bump normal
        float4 bumpNormal = tex2D(bumpMap, IN.Tex1.xy)*2-1;
        // load the projector filter map
        half4 projColor = texCUBE(projMap, IN.Tex4.xyz);
        // load the gloss
        half4 glossColor = tex2D(glossMap, IN.Tex6.xy);
        half a = IN.Color.b*2-1;
        half atten = saturate(a * -a + (1-tex2D(attenMap, IN.Tex5.xy).b));

        // Light vector from normalization cube-map
        float4 lVec = texCUBE(normCubeMapLV, IN.Tex2.xyz)*2-1;
        // Half angle vector from normalization cube-map
        float4 hVec = texCUBE(normCubeMapHA, IN.Tex3.xyz)*2-1;
        
        float NdotL = saturate(dot(lVec.xyz, bumpNormal.xyz));
        float NdotH = saturate(dot(hVec.xyz, bumpNormal.xyz));
        float  specVal = pow(NdotH,8);
        half3 proj = atten * projColor.xyz;
        half3 dif = (decalColor.xyz * NdotL * Diffuse.xyz * proj.xyz) * 2;
        half3 spec = (specVal * glossColor.xyz * Specular.xyz * proj.xyz) * 2;

        // finally add them all together
        OUT.Color.xyz = dif + spec;
        OUT.Color.w = decalColor.w * Diffuse.w;
      }

