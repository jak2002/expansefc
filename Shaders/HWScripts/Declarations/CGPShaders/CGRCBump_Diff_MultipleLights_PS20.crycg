////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Fragment Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGRCBump_Diff_MultipleLights_PS20.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      PS20Only

      MainInput { uniform sampler2D baseMap : texunit0,
                  uniform sampler2D bumpMap : texunit1,
                  uniform float4 Diffuse }
      DeclarationsScript
      {
        struct vertout
        {
          OUT_T0
          OUT_T1
          OUT_T2
        };

        FOUT
      }
      CoreScript
      {
        // load the decal
        half4 decalColor = tex2D(baseMap, IN.Tex0.xy);
        // load the bump normal
        float4 bumpNormal = 2*(tex2D(bumpMap, IN.Tex1.xy)-0.5);

        // normalize post-filtered bump normals
        bumpNormal.xyz = normalize(bumpNormal.xyz);
        // normalize light vector
        float3 lightVec = normalize(IN.Tex2.xyz);
        float fDif = saturate(dot(lightVec.xyz, bumpNormal.xyz));

        half3 dif = (decalColor.xyz * fDif * Diffuse.xyz) * 2;

        // finally add them all together
        OUT.Color.xyz = dif;
        OUT.Color.w = decalColor.w * Diffuse.w;
      }

