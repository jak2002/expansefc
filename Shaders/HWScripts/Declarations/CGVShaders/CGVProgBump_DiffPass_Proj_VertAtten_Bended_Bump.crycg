////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Vertex Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGVProgBump_DiffPass_Proj_VertAtten_Bended_Bump.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"


      VertAttributes { POSITION_3 TEXCOORD0_2 TANG_3X3 PRIM_COLOR SEC_COLOR }
      Param4f (Name = Bend Comp 'ObjWaveX' Comp 'ObjWaveY' Comp 'Bending' Comp = 1)
      MainInput { VIEWPROJ_MATRIX, LIGHT_MATRIX, LIGHT_POS, ATTEN, BEND }
      DeclarationsScript
      {
        struct appin
        {
          IN_P
          IN_T0
          IN_C0
          IN_C1
          IN_TANG
        };
        struct vertout
        {
          OUT_P
          OUT_T0_2
          OUT_T1_2
          OUT_T2
          OUT_T3
          OUT_C0
          OUT_C1
        };

      }
      PositionScript = PosBending
      CoreScript
      {
        OUT.Tex0.xy = IN.TexCoord0.xy;
        OUT.Tex1.xy = IN.TexCoord0.xy; // * float2(2,0.5);
        OUT.Tex3 = mul(LightMatrix, vPos);

        // store normalized light vector
        float3 lightVec = LightPos.xyz - vPos.xyz;
        float fiSqDist = rsqrt(dot(lightVec, lightVec));

        float3x3 objToTangentSpace;
        objToTangentSpace[0] = IN.Tangent;
        objToTangentSpace[1] = IN.Binormal;
        objToTangentSpace[2] = IN.TNormal;
        
        OUT.Tex2.xyz = mul(objToTangentSpace, lightVec.xyz);          
        OUT.Color.xyz = IN.Color.xyz;
        OUT.Color.w = PROC_ATTENVERT;
        OUT.Color1.xyz = IN.Color1.xyz;
# ifdef _HDR
        float ffCameraSpacePosZ = dot(ModelViewProj._31_32_33_34, vPos);
        OUT.Color1.w = clamp((Fog.y - Fog.x*ffCameraSpacePosZ), g_VSCONST_0_025_05_1.x, g_VSCONST_0_025_05_1.w);
# endif        
      }
    