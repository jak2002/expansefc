// ===============================================================
// Vertex Program: geometric refraction
// Description: used for arbytraty geometry refraction
// Last Update: 28/09/2003
// Coder: Tiago Sousa
// ===============================================================

#include "../CGVPMacro.csi"

VertAttributes { POSITION_3 TEXCOORD0_2 }

// setup vertex components
MainInput
{
  // common model view matrix
  VIEWPROJ_MATRIX,  
  uniform float4   ScreenSize,
  uniform float4   LensParams
}

DeclarationsScript
{
  // vertex input
  struct appin
  {
    IN_P
    IN_T0
  };
  struct vertout
  {
    OUT_P
    OUT_T0_2
    OUT_T1_2
    OUT_T2_2
  };

}

// output vertex position
PositionScript = PosCommon

CoreScript
{
  float4 vHPos = mul(ModelViewProj, vPos);     
        
  // output texture coordinates       
      
#ifdef OPENGL
  OUT.Tex1.x = (((vHPos.x / vHPos.w + 1)*0.5-LensParams.x-0.5)*LensParams.z+0.5)*ScreenSize.x; 
  OUT.Tex1.y = (((+vHPos.y / vHPos.w + 1)*0.5+LensParams.y-0.5)*LensParams.z+0.5)*ScreenSize.y; 
#endif

#ifdef D3D  
  OUT.Tex1.x = ((vHPos.x / vHPos.w + 1)*0.5-LensParams.x-0.5)*LensParams.z+0.5; 
  OUT.Tex1.y = ((-vHPos.y / vHPos.w + 1)*0.5-LensParams.y-0.5)*LensParams.z+0.5;
#endif
  
  OUT.Tex2.xy = IN.TexCoord0.xy;
  OUT.Tex0.xy = IN.TexCoord0.xy;

  return OUT;
}
