// ===============================================================
// Vertex Program: Night Vision
// Description: night vision fx
// Last Update: 23/06/2003
// Coder: Tiago Sousa
// ===============================================================

#include "../CGVPMacro.csi"


// setup vertex components
MainInput
{
  // common model view matrix
  uniform float4x4 ModelViewProj,
  
  // screen noise parameters, xy is tilling, zw is offset
  uniform float4   fNoiseParams,
  
  // texture offsets
  uniform float4 vTexCoordScale01,
  uniform float4 vTexCoordScale02, 
  uniform float4 vTexCoordScale03
}

DeclarationsScript
{
  // vertex input
  struct appin
  {
    IN_P
    IN_T0
  };
  struct vertout
  {
    OUT_P
    OUT_T0_2
    OUT_T1_2
    OUT_T2_2
    OUT_T3_2
  };

}

// output vertex position
PositionScript = PosCommon

CoreScript
{
  // output texture coordinates
  float2 vTex = IN.TexCoord0.xy;
  OUT.Tex0.xy = vTex*vTexCoordScale01.xy;
  OUT.Tex1.xy = vTex*vTexCoordScale02.xy;    
  OUT.Tex2.xy = vTex*vTexCoordScale03.xy;
  OUT.Tex3.xy = float2(IN.TexCoord0.x*fNoiseParams.y+fNoiseParams.z, IN.TexCoord0.y*fNoiseParams.y+fNoiseParams.w);
  
  return OUT;
}
