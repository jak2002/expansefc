////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Vertex Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGVProgBeam.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////



      #include "../CGVPMacro.csi"


      MainInput { VIEWPROJ_MATRIX, CAMERA_POS, uniform float4 GeomConstants, uniform float4 StartColor, uniform float4 EndColor, uniform float3 LightForward }
      DeclarationsScript
      {
        struct appin
        {
          IN_P
          IN_T0
          IN_N
        };
        struct vertout
        {
          OUT_P
          OUT_T0_2
          OUT_C0
          OUT_C1
        };

      }
      PositionScript = PosBeam                 
      CoreScript
      {
        // color = Lerp(StartColor, EndColor, fLerp)
        float4 color = lerp(StartColor, EndColor, fLerp);

        float3 camVec = normalize(CameraPos.xyz - vPos.xyz);
        
        float d = dot(camVec.xyz, vNormal.xyz);
        d = d * d;
        d *= min(fLerp*10, vPos.w);
          
        color.w = color.w * d;
          
        OUT.Color = color;
        OUT.Tex0.xy = IN.TexCoord0.xy;
# ifdef _HDR
        float ffCameraSpacePosZ = dot(ModelViewProj._31_32_33_34, vPos);
        OUT.Color1.xyzw = clamp((Fog.y - Fog.x*ffCameraSpacePosZ), g_VSCONST_0_025_05_1.x, g_VSCONST_0_025_05_1.w);
# endif        
      }
    