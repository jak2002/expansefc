////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Vertex Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGVProgBump_DiffSpecPass_Proj_EnvCM.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////


      #include "../CGVPMacro.csi"

      VS20Only

      VertAttributes { POSITION_3 TEXCOORD0_2 TANG_3X3 }
      MainInput { VIEWPROJ_MATRIX, LIGHT_POS, CAMERA_POS, ATTEN, LIGHT_MATRIX, uniform float4x4 TexMatrix }
      DeclarationsScript
      {
        struct appin
        {
          IN_P
          IN_T0
          IN_TANG
        };
        struct vertout
        {
          OUT_P
          OUT_T0_2
          OUT_T1_2
          OUT_T2
          OUT_T3
          OUT_T4
          OUT_T5
          OUT_C0
        };

      }
      PositionScript = PosCommon
      CoreScript
      {
        OUT.Tex0.xy = IN.TexCoord0.xy;
        OUT.Tex1.xy = IN.TexCoord0.xy;
        OUT.Tex4 = mul(LightMatrix, vPos);

        TANG_MATR

        float3 lVec = LightPos.xyz - vPos.xyz;
        float3 lightVec = mul(objToTangentSpace, lVec);          
        OUT.Tex2.xyz = lightVec.xyz;          

        // store normalized light vector
        float fiSqDist = rsqrt(dot(lVec, lVec));        
        OUT.Color.xyz = PROC_ATTENVERT;

        // store normalized light vector
        float3 vVec = CameraPos.xyz - vPos.xyz;
        float3 viewVec = mul(objToTangentSpace, vVec);

        // compute half angle vector
        float3 halfAngleVector = normalize(viewVec) + normalize(lightVec);

        // transform light vector from object space to tangent space and pass it as a tex coords
        OUT.Tex3.xyz = halfAngleVector;

        float3 tCamVec = normalize(vVec);
        float3 tNormal = IN.TNormal.xyz;
        float3 tRef = dot(tNormal.xyz, tCamVec.xyz) * tNormal.xyz * 2 - tCamVec.xyz;
        float4 tRM;
        tRM.xyz = tRef.xyz;
        tRM.w = vPos.w;
        OUT.Tex5 = mul(tRM, TexMatrix);
      }
    