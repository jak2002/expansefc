////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Vertex Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGVProgTerrainShadow_Depth.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////



      #include "../CGVPMacro.csi"

      VS20Only
     
      MainInput { VIEWPROJ_MATRIX, uniform float4 BaseTexGen0, uniform float4 BaseTexGen1, uniform float4x4 ShadowTexGen0, uniform float2x4 ShadowModelMatr, uniform float4 ObjPos, uniform float4 ObjColor, uniform float ShadowBias }
      DeclarationsScript
      {
        struct appin
        {
          IN_P
          IN_C0
          IN_N
        };
        struct vertout
        {
          OUT_P
          OUT_T0_2
          OUT_T1
          OUT_T2_2
          OUT_C0
        };

      }
      PositionScript = PosTerrainOverlay                 
      CoreScript
      {
        OUT.Tex1 = mul(ShadowTexGen0, vPos);
        float fZ = dot(ShadowModelMatr._11_12_13_14, vPos);
        float fW = dot(ShadowModelMatr._21_22_23_24, vPos);
        OUT.Tex2.x = fZ;
        OUT.Tex2.y = fW;
        OUT.Tex0.x = dot(BaseTexGen0, vPos);          
        OUT.Tex0.y = dot(BaseTexGen1, vPos);          
        float fBright = min(1, length(vPos.xyz - ObjPos.xyz) * ObjColor.w);
        float3 vCol = ObjColor.xyz*(vPos.w-fBright) + fBright;
        OUT.Color.xyz = IN.Color.www * vCol;
      }
    