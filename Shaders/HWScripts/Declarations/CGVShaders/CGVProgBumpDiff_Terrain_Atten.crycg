////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Vertex Program
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   CGVProgBumpDiff_Terrain_Atten.crycg
//  Version:     v1.00
//  Created:     11/2/2004 by Andrey Honich.
//  Compilers:   CG/HLSL
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////



      #include "../CGVPMacro.csi"


      VertAttributes { POSITION_3 PRIM_COLOR }
      
      MainInput { VIEWPROJ_MATRIX, LIGHT_POS, uniform float3 Tangent, uniform float3 ScaleBumpTex, uniform float4 TexGen0, uniform float4 TexGen1, ATTEN }
      DeclarationsScript
      {
        struct appin
        {
          IN_P
          IN_C0
          IN_N
        };
        struct vertout
        {
          OUT_P
          OUT_T0_2
          OUT_T1_2
          OUT_T2
          OUT_T3
          OUT_C0
          OUT_C1
        };

      }
      PositionScript = PosTerrainOverlay                 
      CoreScript
      {
        float2 vTex;
        vTex.x = dot(TexGen0, vPos);
        vTex.y = dot(TexGen1, vPos);
        OUT.Tex1.xy = vTex * ScaleBumpTex.xy;
        OUT.Tex0.xy = vTex;
        
        // vlad: tmp fix - normal hardcoded to (0,0,1)
        //float3 normal2;
        //normal2.x=0;
        //normal2.y=0;
        //normal2.z=1;
        
        float3 binorm = normalize(cross(Tangent, normal));
        float3 tang   = cross(binorm, normal);

        // compute the 3x3 tranform from tangent space to object space
        float3x3 objToTangentSpace;
        objToTangentSpace[0] = tang;
        objToTangentSpace[1] = binorm;
        objToTangentSpace[2] = normal;

        // store normalized light vector
        float3 lightVec = mul(objToTangentSpace, LightPos.xyz - vPos.xyz);
        
        float3 vDist = PROC_ATTENPIX;
        // transform light vector from object space to tangent space and pass it as a tex coords
        OUT.Tex3.xyz = vDist;
        OUT.Color1.xyz = vDist;
        
        OUT.Tex2.xyz = lightVec;
        OUT.Color.xyz = IN.Color.w;

# ifdef _HDR
        float ffCameraSpacePosZ = dot(ModelViewProj._31_32_33_34, vPos);
        OUT.Color.w = clamp((Fog.y - Fog.x*ffCameraSpacePosZ), g_VSCONST_0_025_05_1.x, g_VSCONST_0_025_05_1.w);
# endif        
      }
    