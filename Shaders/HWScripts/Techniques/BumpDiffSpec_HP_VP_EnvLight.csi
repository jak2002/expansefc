// Specular bump-mapping for high poly models
// for single and multiple light sources

// Specular lighting pass for directional light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Directional         

    CGPSHader = COMBINER1
        
    CGVProgram = CGVProgBump_DiffSpecPass_HP_EnvLight
    CGVPParam ( Name = EnvColors EnvColor )
    CGVPParam ( Name = TexMatrix TranspObjMatrix )
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )                 
  )
)

// Specular lighting pass for projected directional light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Projected

    CGPShader = COMBINER2
    
    CGVProgram = CGVProgBump_DiffSpecPass_Proj_HP_EnvLight
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = EnvColors EnvColor )
    CGVPParam ( Name = TexMatrix TranspObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = LightMatrix TranspLightMatrix )
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $FromLight
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '3'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )                 
  )
)

// Specular lighting pass for point light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Point         

    CGPShader = COMBINER3
    
    CGVProgram = CGVProgBump_DiffSpecPass_Atten_HP_EnvLight
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = EnvColors EnvColor )
    CGVPParam ( Name = TexMatrix TranspObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )                 
  )
)

