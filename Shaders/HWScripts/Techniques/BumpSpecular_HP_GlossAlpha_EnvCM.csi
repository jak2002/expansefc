  //=========================================================================
  // Shadows support

  HW 'Seq'
  (
    Conditions
    {
      InShadow
      HasDOT3LM
      NoLights
    }

    #define COMBINER CGRCTexDOT3LM_EnvCM
    #define COMBINER1 CGRCTex_EnvCM
    #include "AmbPassDOT3LM_VP_EnvCM.csi"    
    #undefine COMBINER
    #undefine COMBINER1

    #include "ShadowPass4_Neg.csi"
  )

  HW 'Seq'
  (
    Conditions
    {
      InShadow
      HasDOT3LM
      MultipleLights
      SingleLight
    }

    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_DiffSpec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_Spec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER6 CGRCBump_Spec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec_HP_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
    #undefine COMBINER6

    #define COMBINER CGRCTexDOT3LM_EnvCM
    #define COMBINER1 CGRCTex_EnvCM
    #include "AmbPassDOT3LM_VP_EnvCM.csi"    
    #undefine COMBINER
    #undefine COMBINER1

    #include "ShadowPass4_Neg.csi"
  )  

  HW 'Seq'
  (
    Conditions
    {
      InShadow
      SingleLight
    }

    #define COMBINER1 CGRCBump_DiffSpec_SingleLight_HP_NoAtten_GlossAlpha_EnvCM CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Reflect Comp=0 Comp=0 Comp=0 User 'ReflectAmount' )
    #define COMBINER2 CGRCBump_DiffSpec_SingleLight_HP_ProjNoAtten_GlossAlpha_EnvCM CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Reflect Comp=0 Comp=0 Comp=0 User 'ReflectAmount' )
    #define COMBINER3 CGRCBump_DiffSpec_SingleLight_HP_Atten_GlossAlpha_EnvCM CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Reflect Comp=0 Comp=0 Comp=0 User 'ReflectAmount' )
    #define COMBINER4 CGRCBump_Spec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_Spec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER6 CGRCBump_Spec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec_HP_VP_EnvCM.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
    #undefine COMBINER6

    #include "ShadowPass4.csi"
  )

  HW 'Seq'
  (
    Conditions
    {
      InShadow
      MultipleLights
    }

    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_DiffSpec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_Spec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER6 CGRCBump_Spec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec_HP_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
    #undefine COMBINER6

    #define COMBINER CGRCAmbient_EnvCM
    #include "AmbPass_VP_EnvCM.csi"
    #undefine COMBINER

    #include "ShadowPass4.csi"
  )

  //=========================================================================

  // Technique 'NoLights'
  HW 'Seq'
  (
    Conditions
    {
      NoLights
      HasDOT3LM
    }

    // Only Ambient pass
    #define COMBINER CGRCTexDOT3LM_EnvCM
    #define COMBINER1 CGRCTex_EnvCM
    #include "AmbPassDOT3LM_VP_EnvCM.csi"    
    #undefine COMBINER
    #undefine COMBINER1
  )
  // Technique 'NoLights'
  HW 'Seq'
  (
    Conditions
    {
      NoLights
      HasLM
    }

    // Only Ambient pass
    #define COMBINER CGRCTexLM_EnvCM
    #include "AmbPassLM_VP_EnvCM.csi"
    #undefine COMBINER
  )
  HW 'Seq'
  (
    Conditions
    {
      NoLights
    }

    // Only Ambient pass
    #define COMBINER CGRCAmbient_EnvCM
    #include "AmbPass_VP_EnvCM.csi"
    #undefine COMBINER
  )

  // Technique 'Single and Multiple Lights with Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
  	{
      SingleLight
      MultipleLights
      HasDOT3LM
    }

    #define COMBINER CGRCTexDOT3LM_EnvCM
    #define COMBINER1 CGRCTex_EnvCM
    #include "AmbPassDOT3LM_VP_EnvCM.csi"    
    #undefine COMBINER
    #undefine COMBINER1
    
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]' Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_DiffSpec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]' Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]' Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_Spec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER6 CGRCBump_Spec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec_HP_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
    #undefine COMBINER6
  )
  // Technique 'Single and Multiple Lights with Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
  	{
      SingleLight
      MultipleLights
      HasLM
    }

    #define COMBINER CGRCTexLM_EnvCM
    #include "AmbPassLM_VP_EnvCM.csi"
    #undefine COMBINER
    
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_DiffSpec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_Spec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER6 CGRCBump_Spec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec_HP_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
    #undefine COMBINER6
  )

  // Technique 'Single Light without Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
  	{
      SingleLight
    }

    #define COMBINER1 CGRCBump_DiffSpec_SingleLight_HP_NoAtten_GlossAlpha_EnvCM CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Reflect Comp=0 Comp=0 Comp=0 User 'ReflectAmount' )
    #define COMBINER2 CGRCBump_DiffSpec_SingleLight_HP_ProjNoAtten_GlossAlpha_EnvCM CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Reflect Comp=0 Comp=0 Comp=0 User 'ReflectAmount' )
    #define COMBINER3 CGRCBump_DiffSpec_SingleLight_HP_Atten_GlossAlpha_EnvCM CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Reflect Comp=0 Comp=0 Comp=0 User 'ReflectAmount' )
    #define COMBINER4 CGRCBump_Spec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_Spec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER6 CGRCBump_Spec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec_HP_VP_EnvCM.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
    #undefine COMBINER6
  )

  // Technique 'Multiple Lights without Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
  	{
      MultipleLights
    }

    #define COMBINER CGRCAmbient_EnvCM
    #include "AmbPass_VP_EnvCM.csi"
    #undefine COMBINER
    
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_DiffSpec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_MultipleLights_HP_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_Spec_MultipleLights_HP_ProjNoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER6 CGRCBump_Spec_MultipleLights_HP_NoAtten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec_HP_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
    #undefine COMBINER6
  )

