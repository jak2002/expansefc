
// Specular lighting pass for directional light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Directional         
    LightType = OnlySpecular

    CGPSHader = COMBINER0
        
    CGVProgram = CGVProgBump_SpecPass
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')

    Layer '0'
    (
      Map = $Gloss
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      SecondPassRendState
      {
        DepthWrite = 0
        Blend (ONE ONE)
      }
    )                 
  )
)

// Specular lighting pass for directional light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Directional         

    CGPSHader = COMBINER1
        
    CGVProgram = CGVProgBump_DiffSpec_Gloss
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )
    Layer '3'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '4'
    (
      Map = $Gloss
      TexColorOp = NoSet
      SecondPassRendState
      {
        DepthWrite = 0
        Blend (ONE ONE)
      }
    )                 
  )
)

//======================================================================

// Specular lighting pass for projected light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Projected
    LightType = OnlySpecular

    CGPSHader = COMBINER2
        
    CGVProgram = CGVProgBump_SpecPass_ProjAtten_Gloss_VS20
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = LightMatrix TranspLightMatrix )

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
    )                 
    Layer '3'
    (
      Map = $FromLight
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '4'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
    )                 
    Layer '5'
    (
      Map = $Gloss
      TexColorOp = NoSet
      SecondPassRendState
      {
        DepthWrite = 0
        DepthFunc = Equal
        Blend (ONE ONE)
      }
    )                 
  )
)

// Specular lighting pass for projected light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Projected

    CGPSHader = COMBINER3
        
    CGVProgram = CGVProgBump_DiffSpecPass_ProjAtten_Gloss_VS20
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)
    CGVPParam ( Name = LightMatrix TranspLightMatrix )

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )
    Layer '3'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '4'
    (
      Map = $FromLight
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '5'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
    )                 
    Layer '6'
    (
      Map = $Gloss
      TexColorOp = NoSet
      SecondPassRendState
      {
        DepthWrite = 0
        DepthFunc = Equal
        Blend (ONE ONE)
      }
    )                 
  )
)

//======================================================================

// Specular lighting pass for Point light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Point
    LightType = OnlySpecular

    CGPSHader = COMBINER4
        
    CGVProgram = CGVProgBump_SpecPass_Atten
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Gloss
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
    )                 
    Layer '3'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
      SecondPassRendState
      {
        DepthWrite = 0
        Blend (ONE ONE)
      }
    )                 
  )
)

// Specular lighting pass for point light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Point

    CGPSHader = COMBINER5
        
    CGVProgram = CGVProgBump_DiffSpecPass_Atten_Gloss
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )
    Layer '3'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '4'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
    )                 
    Layer '5'
    (
      Map = $Gloss
      TexColorOp = NoSet
      SecondPassRendState
      {
        DepthWrite = 0
        Blend (ONE ONE)
      }
    )                 
  )
)
