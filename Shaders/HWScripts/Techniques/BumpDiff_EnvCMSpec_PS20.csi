// Diffuse bump-mapping

Light
(         
  ShadeLayer
  (
    LightType = Directional         

    CGPShader = COMBINER0

    CGVProgram = CGVProgBump_DiffPass_EnvCMSpec_VS20
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = ModelMatrix TranspObjMatrix )

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $ENVCMAP
      TexType = CubeMap
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )               
  )
)

// Diffuse lighting for projected light source
Light
(         
  ShadeLayer
  (
    LightType = Projected

    CGPShader = COMBINER1

    CGVProgram = CGVProgBump_DiffPass_Proj_EnvCMSpec_VS20
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = LightMatrix TranspLightMatrix )
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = ModelMatrix TranspObjMatrix )

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )               
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $FromLight
      TexColorOp = NoSet
    )                 
    Layer '3'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
    )                 
    Layer '4'
    (
      Map = $ENVCMAP
      TexType = CubeMap
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )               
  )
)

// Diffuse lighting for point light source
Light
(         
  ShadeLayer
  (
    LightType = Point

    CGPShader = COMBINER2 

    CGVProgram = CGVProgBump_DiffPass_Atten_EnvCMSpec_VS20
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = ModelMatrix TranspObjMatrix )

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
    )                 
    Layer '3'
    (
      Map = $ENVCMAP
      TexType = CubeMap
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )               
  )
)