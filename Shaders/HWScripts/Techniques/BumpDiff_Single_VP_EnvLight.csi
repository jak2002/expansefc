// Diffuse bump-mapping
// for single light source

// Diffuse lighting for directional light source
Light
(         
  /*ShadeLayer
  (
    LightType = Directional         
    NoBump

    CGPShader = CGRCBump_Diff_SingleLight_EnvLight
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )

    CGVProgram = CGVProgBump_DiffPass_EnvLight
    CGVPParam ( Name = EnvColors EnvColor )
    CGVPParam ( Name = TexMatrix TranspObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)

    Layer '0'
    (
      Map = $Diffuse
      TexGen = Base
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )                 
  )*/
  ShadeLayer
  (
    LightType = Directional         

    CGPShader = CGRCBump_Diff_SingleLight_EnvLight
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]'  Comp 'Opacity' )

    CGVProgram = CGVProgBump_DiffPass_EnvLight
    CGVPParam ( Name = EnvColors EnvColor )
    CGVPParam ( Name = TexMatrix TranspObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )                 
  )
)

// Diffuse lighting for projected light source
#ifdef SUPPORT_PROFILE_PS_1_1
Light
(         
  ShadeLayer
  (
    LightType = Projected

    CGPShader = CGRCBump_Diff_ProjSingleLight_NoAtten_EnvLight
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )

    CGVProgram = CGVProgBump_DiffPass_Proj_EnvLight
    CGVPParam ( Name = EnvColors EnvColor )
    CGVPParam ( Name = TexMatrix TranspObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = LightMatrix TranspLightMatrix )
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )               
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
	  )                 
    Layer '3'
    (
      Map = $FromLight
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )                 
  )
)
#endif
#ifdef OTHER
Light
(         
  ShadeLayer
  (
    LightType = Projected

    CGPShader = CGRCBump_Diff_ProjSingleLight_Atten_EnvLight_PS20
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )

    CGVProgram = CGVProgBump_DiffPass_Proj_EnvLight
    CGVPParam ( Name = EnvColors EnvColor )
    CGVPParam ( Name = TexMatrix TranspObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = LightMatrix TranspLightMatrix )
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )               
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
	  )                 
    Layer '3'
    (
      Map = $FromLight
      TexColorOp = NoSet
    )                 
    Layer '4'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
      SecondPassRendState
      {
        DepthWrite = 0
        DepthFunc = Equal
        Blend (ONE ONE)
      }
    )                 
  )
)
#endif

// Diffuse lighting for point light source
Light
(         
  ShadeLayer
  (
    LightType = Point

    CGPShader = CGRCBump_Diff_SingleLight_Atten_EnvLight 
    CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' )
    CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )

    CGVProgram = CGVProgBump_DiffPass_Atten_EnvLight
    CGVPParam ( Name = EnvColors EnvColor )
    CGVPParam ( Name = TexMatrix TranspObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexType = Bump    
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '3'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )                 
  )
)
