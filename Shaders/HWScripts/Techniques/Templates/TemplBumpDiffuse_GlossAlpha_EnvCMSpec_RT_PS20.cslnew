; Shaders Script file
; Copyright (c) 2001-2003 Crytek Studios. All Rights Reserved.
; Author: Honich Andrey

Version (1.00)

//========================================================================
// ATI R3xx / NVidia NV3X (PS.2.0 / PS.2.X only)

// Diffuse bump-mapping with fresnel real-time env CM reflections (alpha channel of diffuse texture)
// At least two passes for multiple light sources
// One pass for single light source

// Supports:
// 1. Dot3 light maps
// 2. Simple light maps
// 3. Three types of shadow maps (2D, Depth-maps and mixed Depth/2D)
// 4. Stencil shadows
// 5. Three types of light sources (Directional, Point/Omni, Projected)
// 6. Optimised separate techniques for Single/Multiple light sources

/*Shader 'TemplBumpDiffuse_GlossAlpha_EnvCMSpec_RT_PS20'
(
   Params
  {
    Sort = Opaque
  }
  Public
  (
    float 'EnvMapAmount' (1)
    float 'FresnelScale' (1)
    float 'FresnelBias' (0)
    float 'FresnelPow' (5)
  )
  RenderParams
  {
    DrawFarSprites
    DrawStaticObjects
    DrawParticles
    DrawEntities
    DrawIndoors
    DrawDetailTextures
    FullDetailTerrain
    DrawWater
    DrawTerrain
  }

  #define $ENVCMAP $EnvironmentCubeMap
  #include "BumpDiffuse_GlossAlpha_EnvCMSpec_PS20.csi"
  #undefine $ENVCMAP
)*/

Shader 'TemplBumpDiffuse_GlossAlpha_EnvCMSpec_RT_PS20'
(
  Params
  {
    Sort = Opaque
  }

  #define %DIFFUSE 0x1
  #define %DIFFUSE_PERPIXEL 0x8000
  #define %GLOSS_DIFFUSEALPHA 0x20
  #define %ENVCMSPEC 0x80
  #define %PROJLIGHT_PERPIXELATTEN 0x100
  #define %RTCUBEMAP 0x10000
  #define %BUMP_MAP 0x1000
  
#ifdef PS30|PS2X
  #include "../IllumTemplate_PS30.csi"
#endif
#ifdef OTHER
  #include "../IllumTemplate.csi"
#endif
)

//=========================================================================================

