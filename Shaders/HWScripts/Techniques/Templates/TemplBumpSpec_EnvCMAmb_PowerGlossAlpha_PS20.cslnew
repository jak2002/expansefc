; Shaders Script file
; Copyright (c) 2001-2003 Crytek Studios. All Rights Reserved.
; Author: Honich Andrey

Version (1.00)

//========================================================================
// ATI R3xx / NVidia NV3X (PS.2.0 / PS.2.X only)

// Specular & Diffuse bump-mapping with env. CM reflections
// and with specular exp. in alpha channel of gloss texture
// At least two passes for multiple light sources
// One pass for single light source

// Supports:
// 1. Dot3 light maps
// 2. Simple light maps
// 3. Three types of shadow maps (2D, Depth-maps and mixed Depth/2D)
// 4. Stencil shadows
// 5. Three types of light sources (Directional, Point/Omni, Projected)
// 6. Optimised separate techniques for Single/Multiple light sources

/*Shader 'TemplBumpSpec_EnvCMAmb_PowerGlossAlpha_PS20'
(
  Params
  (
    Sort = Opaque
  )
  Public
  (
    float 'ReflectAmount' (0.25)
    float 'SpecularExpMultiplier' (255)
  )

  #define $ENVCMAP $CubeMap
  #include "BumpSpecular_EnvCM_PowerGlossAlpha_PS20.csi"  
  #undefine $ENVCMAP
)*/

Shader 'TemplBumpSpec_EnvCMAmb_PowerGlossAlpha_PS20'
(
  Params
  (
    Sort = Opaque
  )

  #define %DIFFUSE 0x1
  #define %DIFFUSE_PERPIXEL 0x8000
  #define %SPECULAR 0x2
  #define %SPECULAR_PERPIXEL 0x400
  #define %BUMP_NORMALIZE 0x200
  #define %ENVCMAMB 0x40
  #define %SPECULARPOW_GLOSSALPHA 0x800
  #define %PROJLIGHT_PERPIXELATTEN 0x100
  #define %BUMP_MAP 0x1000

#ifdef PS30|PS2X
  #include "../IllumTemplate_PS30.csi"
#endif
#ifdef OTHER
  #include "../IllumTemplate.csi"
#endif
)
