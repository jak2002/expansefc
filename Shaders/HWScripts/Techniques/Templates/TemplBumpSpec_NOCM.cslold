; Shaders Script file
; Copyright (c) 2001-2003 Crytek Studios. All Rights Reserved.
; Author: Honich Andrey

Version (1.00)

//========================================================================
// ATI R2xx / NVidia NV2X (PS.1.X and above)

// Specular and diffuse bump-mapping
// without normalization cube-maps: using pixel shader instruction dp3x2
// One pass for single light source (one pass for each additional LS)
Shader 'TemplBumpSpec_NOCM'
(
  Params
  (
    Sort = Opaque
  )
  

  //=========================================================================

  // Technique 'NoLights'
  HW 'Seq'
  (
    Conditions
    {
      NoLights
    }

    // Only Ambient pass
    #define COMBINER CGRCAmbient
    #include "../AmbPass_VP.csi"
    #undefine COMBINER
  )

  HW 'Seq'
  (
    Conditions
    {
      SingleLight
    }

    Light 'Specular'
    (
      ShadeLayer
      (
        CGVProgram = CGVProgBump_DiffSpec_NOCM
        CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
        CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')

        CGPShader = CGRCBump_DiffSpec_SingleLight_PS
        CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp = 1 )
        CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'LightColor[3]' )
        CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp = 1 )

        Layer '0'
        (
          Map = $Bump
          TexType = Bumpmap
          TexColorOp = NoSet
        )

        Layer '1'
        (
          Map = $None
          TexColorOp = NoSet
        )

        Layer '2'
        (
          Map = $Phong
          TexColorOp = NoSet
        )

        Layer '3'
        (
          Map = $Diffuse
          TexColorOp = NoSet
        )
      )
    )
  )

  HW 'Seq'
  (
    Conditions
    {
      MultipleLights
    }

    // Only Ambient pass
    #define COMBINER CGRCAmbient
    #include "../AmbPass_VP.csi"
    #undefine COMBINER
    
    Light 'Specular'
    (
      ShadeLayer
      (
        CGVProgram = CGVProgBump_DiffSpec_NOCM
        CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
        CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')

        CGPShader = CGRCBump_DiffSpec_MultipleLight_PS
        CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp = 1 )
        CGPSParam ( Name = Diffuse  Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'LightColor[3]' )

        Layer '0'
        (
          Map = $Bump
          TexType = Bumpmap
          TexColorOp = NoSet
        )

        Layer '1'
        (
          Map = $None
          TexColorOp = NoSet
        )

        Layer '2'
        (
          Map = $Phong
          TexColorOp = NoSet
        )

        Layer '3'
        (
          SecondPassRendState
          {
            DepthWrite = 0
            DepthFunc = Equal

            Blend (ONE ONE)
          }
          Map = $Diffuse
          TexColorOp = NoSet
        )
      )
    )
  )
)


//===============================================================================================

Shader 'TemplBrushedMetal'
(
  Params
  (
    //Cull = TwoSided
    //TessSize = 16
  )
  // Technique 'NoLights'
  HW 'Seq'
  (
    Conditions
    {
      NoLights
    }

    // Only Ambient pass
    ShadeLayer
    (         
      Conditions
      {
        Ambient
      }

      CGVProgram = CGVProgSimple_Tex

      CGPShader = CGRCAmbient
      CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp=1 )

      Layer '0'
      (
        Map = $Diffuse
        TexColorOp = NoSet
      )               
    )
  )

  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
    }

    FirstLight 'Specular'
    (
      ShadeLayer
      (
        CGVProgram = CGVProg_BrushedMetal
        CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
        CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')

        CGPShader = CGRCBrushedMetal
        CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'LightColor[3]' )
        CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity' )
        CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp = 1 )
        Layer '0'
        (
          Map = $Bump
          TexType = Bumpmap
          TexColorOp = NoSet
        )
        Layer '1'
        (
          Map = $None
          TexColorOp = NoSet
        )
        Layer '2'
        (
          Map = Defaults/silvergradient
          ClampTexCoords
          TexColorOp = NoSet
        )
        Layer '3'
        (
          Map = $Diffuse
          TexColorOp = NoSet
        )
      )
    )
  )
)

//====================================================================================================

Shader 'BumpSunGlow'
(
  Public
  (
    float 'WaveAmplitude' (0.0)
  )
  Params
  (
    //Cull = TwoSided
    Sort = WaterBeach
    //PolygonOffset
    //TessSize = 16
  )
#ifdef NV1X
  HW
  (    
    ShadeLayer
    (      
    )
  )
#endif
#ifdef OTHER
  HW 'Seq'
  (
    Light
    (         
      ShadeLayer
      (
        CGVProgram = CGVProgBumpSunGlow
        CGVPParam ( Name = NoisePos Comp 'time 0.6' Comp 'time 0.6' Comp = 0 User 'WaveAmplitude') // xy = wavepos, z = 0, w = amplitude
        CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2' Comp = 0.15)
        CGVPParam ( Name = TexShiftRipple Comp 'time 0.06' Comp 'time 0.12')  // xy = ripple uv ADDer
        CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
        CGVPParam ( Name = TexGenRipple0 Comp = 0.125 Comp = 0 Comp = 0 Comp 'CameraPos pos 0 / 8')  // Ripple texgen
        CGVPParam ( Name = TexGenRipple1 Comp = 0 Comp = 0.125 Comp = 0 Comp 'CameraPos pos 1 / 8')  // Ripple texgen
        CGVPParam ( Name = Normal Comp = 0 Comp = 0 Comp = 1)  // Normal
        CGVPParam ( Name = Tangent Comp = 0 Comp = -1 Comp = 0 ) // Tangent

        CGPShader = CGRCBumpSunGlow
        CGPSParam ( Name = Specular Comp 'SpecLightColor[0]'  Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp = 1 )

        Layer '0'
        (
          TexType = Bumpmap
          Map = test8
          /*BumpHighRes
          BumpAmount = 10
          TexLodBias = 1
          Sequence (Maps = textures/animated/water/caust## Time = 0.05 Loop)*/
          TexColorOp = NoSet
        )

        Layer '1'
        (
          Map = $None
          TexColorOp = NoSet
        )

        Layer '2'
        (
          //Blend 'ONE ONE'        
          Map = $Phong100
          TexLodBias = 1
          TexColorOp = NoSet
        )
      )
    )
  )
#endif
)
