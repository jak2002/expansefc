
  // Tech 0
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      HasDOT3LM
      NoLights
    }

    #define COMBINER CGRCTexDOT3LM_EnvCM
    #define COMBINER1 CGRCTex_EnvCM
    #include "AmbPassDOT3LM_VP_EnvCM.csi"    
    #undefine COMBINER
    #undefine COMBINER1

    #include "ShadowPass4_Neg.csi"
  )

#ifdef SUPPORT_PROFILE_PS_1_1
  // Tech 1
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      HasDOT3LM
      MultipleLights
      SingleLight
    }

    #include "BumpDiff_Multiple_VP.csi"

    #define COMBINER1 CGRCBump_Spec_GlossAlpha
    #define COMBINER2 CGRCBump_Spec_Proj_GlossAlpha
    #define COMBINER3 CGRCBump_Spec_Atten_GlossAlpha
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3

    #define COMBINER CGRCTexDOT3LM_EnvCM
    #define COMBINER1 CGRCTex_EnvCM
    #include "AmbPassDOT3LM_VP_EnvCM.csi"    
    #undefine COMBINER
    #undefine COMBINER1

    #include "ShadowPass4_Neg.csi"
  )
#endif
#ifdef OTHER
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      HasDOT3LM
      SingleLight
      MultipleLights
    }

    #define COMBINER0 CGRCBump_Spec_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5

    #define COMBINER CGRCTexDOT3LM_EnvCM
    #define COMBINER1 CGRCTex_EnvCM
    #include "AmbPassDOT3LM_VP_EnvCM.csi"    
    #undefine COMBINER
    #undefine COMBINER1

    #include "ShadowPass4_Neg.csi"
  )
#endif

#ifdef SUPPORT_PROFILE_PS_1_1
  // Tech 2
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      MultipleLights
      SingleLight
      NoLights
    }

    #include "BumpDiff_Multiple_VP.csi"
    
    #define COMBINER1 CGRCBump_Spec_GlossAlpha
    #define COMBINER2 CGRCBump_Spec_Proj_GlossAlpha
    #define COMBINER3 CGRCBump_Spec_Atten_GlossAlpha
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3

    #define COMBINER CGRCAmbient_EnvCM
    #include "AmbPass_VP_EnvCM.csi"
    #undefine COMBINER

    #include "ShadowPass4.csi"
  )
#endif
#ifdef OTHER
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      SingleLight
    }

    #define COMBINER0 CGRCBump_Spec_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_SingleLight_GlossAlpha_EnvCM CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Reflect User 'ReflectAmount' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_SingleLight_ProjAtten_GlossAlpha_EnvCM_PS20 CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Reflect User 'ReflectAmount' )
    #define COMBINER4 CGRCBump_Spec_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_SingleLight_Atten_GlossAlpha_EnvCM CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Reflect User 'ReflectAmount' )
    #include "BumpDiffSpec_EnvCM.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5

    #include "ShadowPass4.csi"
  )
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      MultipleLights
    }

    #define COMBINER0 CGRCBump_Spec_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5

    #define COMBINER CGRCAmbient_EnvCM
    #include "AmbPass_VP_EnvCM.csi"
    #undefine COMBINER

    #include "ShadowPass4.csi"
  )
#endif

  // Technique 'NoLights'
  // Tech 3
  HW 'Seq'
  (
    Conditions
    {
      NoLights
      HasDOT3LM
    }

    #define COMBINER CGRCTexDOT3LM_EnvCM
    #define COMBINER1 CGRCTex_EnvCM
    #include "AmbPassDOT3LM_VP_EnvCM.csi"    
    #undefine COMBINER
    #undefine COMBINER1
  )

  // Technique 'NoLights'
  // Tech 4
  HW 'Seq'
  (
    Conditions
    {
      NoLights
      HasLM
    }

    // Only Ambient pass
    #define COMBINER CGRCTexLM_EnvCM
    #include "AmbPassLM_VP_EnvCM.csi"
    #undefine COMBINER
  )

  // Tech 5
  HW 'Seq'
  (
    Conditions
    {
      NoLights
    }

    // Only Ambient pass
    #define COMBINER CGRCAmbient_EnvCM
    #include "AmbPass_VP_EnvCM.csi"
    #undefine COMBINER
  )

#ifdef SUPPORT_PROFILE_PS_1_1
  // Technique 'Single and Multiple Lights with Lightmaps' (optimization)
  // Tech 6
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
      HasDOT3LM
    }

    #define COMBINER CGRCTexDOT3LM_EnvCM
    #define COMBINER1 CGRCTex_EnvCM
    #include "AmbPassDOT3LM_VP_EnvCM.csi"    
    #undefine COMBINER
    #undefine COMBINER1
    
    #include "BumpDiff_Multiple_VP.csi"
    #define COMBINER1 CGRCBump_Spec_GlossAlpha
    #define COMBINER2 CGRCBump_Spec_Proj_GlossAlpha
    #define COMBINER3 CGRCBump_Spec_Atten_GlossAlpha
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
  )

  // Technique 'Single and Multiple Lights with Lightmaps' (optimization)
  // Tech 7
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
      HasLM
    }

    #define COMBINER CGRCTexLM_EnvCM
    #include "AmbPassLM_VP_EnvCM.csi"
    #undefine COMBINER
    
    #include "BumpDiff_Multiple_VP.csi"
    #define COMBINER1 CGRCBump_Spec_GlossAlpha
    #define COMBINER2 CGRCBump_Spec_Proj_GlossAlpha
    #define COMBINER3 CGRCBump_Spec_Atten_GlossAlpha
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
  )

  // Technique 'Single and Multiple Lights without Lightmaps' (optimization)
  // Tech 8
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
    }

    #define COMBINER CGRCAmbient_EnvCM
    #include "AmbPass_VP_EnvCM.csi"
    #undefine COMBINER
    
    #include "BumpDiff_Multiple_VP.csi"
    #define COMBINER1 CGRCBump_Spec_GlossAlpha
    #define COMBINER2 CGRCBump_Spec_Proj_GlossAlpha
    #define COMBINER3 CGRCBump_Spec_Atten_GlossAlpha
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
  )
#endif  

#ifdef OTHER
  // Technique 'Single and Multiple Lights with Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
      HasDOT3LM
    }

    #define COMBINER CGRCTexDOT3LM_EnvCM
    #define COMBINER1 CGRCTex_EnvCM
    #include "AmbPassDOT3LM_VP_EnvCM.csi"    
    #undefine COMBINER
    #undefine COMBINER1

    #define COMBINER0 CGRCBump_Spec_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
  )

  // Technique 'Single and Multiple Lights with Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
      HasLM
    }

    // Only Ambient pass
    #define COMBINER CGRCTexLM_EnvCM
    #include "AmbPassLM_VP_EnvCM.csi"
    #undefine COMBINER
    
    #define COMBINER0 CGRCBump_Spec_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
  )

  HW 'Seq'
  (
    Conditions
    {
      SingleLight
    }

    #define COMBINER0 CGRCBump_Spec_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_SingleLight_GlossAlpha_EnvCM CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Reflect User 'ReflectAmount' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_SingleLight_ProjAtten_GlossAlpha_EnvCM_PS20 CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Reflect User 'ReflectAmount' )
    #define COMBINER4 CGRCBump_Spec_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_SingleLight_Atten_GlossAlpha_EnvCM CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Reflect User 'ReflectAmount' )
    #include "BumpDiffSpec_EnvCM.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
  )

  // Technique 'Single and Multiple Lights without Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      MultipleLights
    }

    #define COMBINER CGRCAmbient_EnvCM
    #include "AmbPass_VP_EnvCM.csi"
    #undefine COMBINER
    
    #define COMBINER0 CGRCBump_Spec_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_GlossAlpha_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten_GlossAlpha CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten_GlossAlpha CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
  )

#endif  
