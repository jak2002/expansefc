
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      HasDOT3LM
      NoLights
    }

    #define COMBINER CGRCTexDOT3LM
    #include "AmbPassDOT3LM_VP.csi"    
    #undefine COMBINER

    #include "ShadowPass4_Neg.csi"
  )

#ifdef SUPPORT_PROFILE_PS_1_1
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      HasDOT3LM
      MultipleLights
      SingleLight
    }

    #include "BumpDiff_Multiple_VP.csi"
    #define COMBINER1 CGRCBump_Spec
    #define COMBINER2 CGRCBump_Spec_Proj
    #define COMBINER3 CGRCBump_Spec_Atten
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3

    #define COMBINER CGRCTexDOT3LM
    #include "AmbPassDOT3LM_VP.csi"    
    #undefine COMBINER

    #include "ShadowPass4_Neg.csi"
  )
#endif
#ifdef OTHER
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      HasDOT3LM
      MultipleLights
      SingleLight
    }

    #define COMBINER0 CGRCBump_Spec CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5

    #define COMBINER CGRCTexDOT3LM
    #include "AmbPassDOT3LM_VP.csi"    
    #undefine COMBINER

    #include "ShadowPass4_Neg.csi"
  )
#endif

#ifdef SUPPORT_PROFILE_PS_1_1
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      MultipleLights
      SingleLight
      NoLights
      HasEnvLCMap
    }

    #include "BumpDiff_Multiple_VP.csi"    
    #define COMBINER1 CGRCBump_Spec
    #define COMBINER2 CGRCBump_Spec_Proj
    #define COMBINER3 CGRCBump_Spec_Atten
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3

    #define COMBINER CGRCAmbient_EnvLight
    #include "AmbPass_VP_EnvLight.csi"
    #undefine COMBINER

    #include "ShadowPass4_EnvLight.csi"
  )
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      MultipleLights
      SingleLight
      NoLights
    }

    #include "BumpDiff_Multiple_VP.csi"    
    #define COMBINER1 CGRCBump_Spec
    #define COMBINER2 CGRCBump_Spec_Proj
    #define COMBINER3 CGRCBump_Spec_Atten
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3

    #define COMBINER CGRCAmbient
    #include "AmbPass_VP.csi"
    #undefine COMBINER

    #include "ShadowPass4.csi"
  )
#endif
#ifdef OTHER
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      SingleLight
      HasEnvLCMap
    }

    #define COMBINER1 CGRCBump_DiffSpec_SingleLight_EnvLight CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_SingleLight_ProjAtten_EnvLight_PS20 CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_SingleLight_Atten_EnvLight CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec_EnvLight.csi"
    #undefine COMBINER1
    #undefine COMBINER3
    #undefine COMBINER5

    #include "ShadowPass4_EnvLight.csi"
  )
  HW 'Seq'
  (
    Conditions
    {
      InShadow
      SingleLight
    }

    #define COMBINER0 CGRCBump_Spec CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_SingleLight CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_SingleLight_ProjAtten_PS20 CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_SingleLight_Atten CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5

    #include "ShadowPass4.csi"
  )

  HW 'Seq'
  (
    Conditions
    {
      InShadow
      MultipleLights
      HasEnvLCMap
    }

    #define COMBINER0 CGRCBump_Spec CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5

    #define COMBINER CGRCAmbient_EnvLight
    #include "AmbPass_VP_EnvLight.csi"
    #undefine COMBINER

    #include "ShadowPass4_EnvLight.csi"
  )

  HW 'Seq'
  (
    Conditions
    {
      InShadow
      MultipleLights
    }

    #define COMBINER0 CGRCBump_Spec CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5

    #define COMBINER CGRCAmbient
    #include "AmbPass_VP.csi"
    #undefine COMBINER

    #include "ShadowPass4.csi"
  )
#endif

  HW 'Seq'
  (
    Conditions
    {
      NoLights
      HasDOT3LM
    }

    // Only Ambient pass
    #define COMBINER CGRCTexDOT3LM
    #include "AmbPassDOT3LM_VP.csi"
    #undefine COMBINER
  )
  
  // Technique 'NoLights'
  HW 'Seq'
  (
    Conditions
    {
      NoLights
      HasLM
    }

    // Only Ambient pass
    #define COMBINER CGRCTexLM
    #include "AmbPassLM_VP.csi"
    #undefine COMBINER
  )

  HW 'Seq'
  (
    Conditions
    {
      NoLights
      HasEnvLCMap
    }

    // Only Ambient pass
    #define COMBINER CGRCAmbient_EnvLight
    #include "AmbPass_VP_EnvLight.csi"
    #undefine COMBINER
  )

  HW 'Seq'
  (
    Conditions
    {
      NoLights
    }

    // Only Ambient pass
    #define COMBINER CGRCAmbient
    #include "AmbPass_VP.csi"
    #undefine COMBINER
  )

#ifdef SUPPORT_PROFILE_PS_1_1
  // Technique 'Single and Multiple Lights with Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
      HasDOT3LM
    }

    #define COMBINER CGRCTexDOT3LM
    #include "AmbPassDOT3LM_VP.csi"
    #undefine COMBINER
    
    #include "BumpDiff_Multiple_VP.csi"
    #define COMBINER1 CGRCBump_Spec
    #define COMBINER2 CGRCBump_Spec_Proj
    #define COMBINER3 CGRCBump_Spec_Atten
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
  )

  // Technique 'Single and Multiple Lights with Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
      HasLM
    }

    #define COMBINER CGRCTexLM
    #include "AmbPassLM_VP.csi"
    #undefine COMBINER
    
    #include "BumpDiff_Multiple_VP.csi"
    #define COMBINER1 CGRCBump_Spec
    #define COMBINER2 CGRCBump_Spec_Proj
    #define COMBINER3 CGRCBump_Spec_Atten
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
  )

  // Technique 'Single and Multiple Lights without Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
      HasEnvLCMap
    }

    #define COMBINER CGRCAmbient_EnvLight
    #include "AmbPass_VP_EnvLight.csi"
    #undefine COMBINER
    
    #include "BumpDiff_Multiple_VP.csi"
    
    #define COMBINER1 CGRCBump_Spec
    #define COMBINER2 CGRCBump_Spec_Proj
    #define COMBINER3 CGRCBump_Spec_Atten
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
  )

  // Technique 'Single and Multiple Lights without Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
    }

    #define COMBINER CGRCAmbient
    #include "AmbPass_VP.csi"
    #undefine COMBINER
    
    #include "BumpDiff_Multiple_VP.csi"
    
    #define COMBINER1 CGRCBump_Spec
    #define COMBINER2 CGRCBump_Spec_Proj
    #define COMBINER3 CGRCBump_Spec_Atten
    #include "BumpSpec_VP.csi"
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
  )
#endif

#ifdef OTHER
  // Technique 'Single and Multiple Lights with Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
      HasDOT3LM
    }

    #define COMBINER CGRCTexDOT3LM
    #include "AmbPassDOT3LM_VP.csi"
    #undefine COMBINER

    #define COMBINER0 CGRCBump_Spec CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
  )

  // Technique 'Single and Multiple Lights with Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      MultipleLights
      HasLM
    }

    #define COMBINER CGRCTexLM
    #include "AmbPassLM_VP.csi"
    #undefine COMBINER
    
    #define COMBINER0 CGRCBump_Spec CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
  )

  // Technique 'Single and Multiple Lights without Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
      HasEnvLCMap
    }

    #define COMBINER1 CGRCBump_DiffSpec_SingleLight_EnvLight CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_SingleLight_ProjAtten_EnvLight_PS20 CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_SingleLight_Atten_EnvLight CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec_EnvLight.csi"
    #undefine COMBINER1
    #undefine COMBINER3
    #undefine COMBINER5
  )

  // Technique 'Single and Multiple Lights without Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      SingleLight
    }

    #define COMBINER0 CGRCBump_Spec CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_SingleLight CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_SingleLight_ProjAtten_PS20 CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_SingleLight_Atten CGPSParam ( Name = Ambient Comp 'AmbLightColor[0]'  Comp 'AmbLightColor[1]' Comp 'AmbLightColor[2]' Comp 'Opacity') CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
  )

  // Technique 'Single and Multiple Lights without Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      MultipleLights
      HasEnvLCMap
    }

    #define COMBINER CGRCAmbient_EnvLight
    #include "AmbPass_VP_EnvLight.csi"
    #undefine COMBINER
    
    #define COMBINER0 CGRCBump_Spec CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
  )

  // Technique 'Single and Multiple Lights without Lightmaps' (optimization)
  HW 'Seq'
  (
    Conditions
    {
      MultipleLights
    }

    #define COMBINER CGRCAmbient
    #include "AmbPass_VP.csi"
    #undefine COMBINER
    
    #define COMBINER0 CGRCBump_Spec CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER1 CGRCBump_DiffSpec_MultipleLights CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER2 CGRCBump_Spec_ProjAtten_PS20 CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER3 CGRCBump_DiffSpec_MultipleLights_ProjAtten_PS20 CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER4 CGRCBump_Spec_Atten CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #define COMBINER5 CGRCBump_DiffSpec_MultipleLights_Atten CGPSParam ( Name = Diffuse Comp 'LightColor[0]'  Comp 'LightColor[1]' Comp 'LightColor[2]' Comp 'Opacity' ) CGPSParam ( Name = Specular Comp 'SpecLightColor[0]' Comp 'SpecLightColor[1]' Comp 'SpecLightColor[2]' Comp 'Opacity' )
    #include "BumpDiffSpec.csi"
    #undefine COMBINER0
    #undefine COMBINER1
    #undefine COMBINER2
    #undefine COMBINER3
    #undefine COMBINER4
    #undefine COMBINER5
  )
#endif  
