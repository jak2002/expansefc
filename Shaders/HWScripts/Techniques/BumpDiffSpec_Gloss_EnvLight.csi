
// Specular lighting pass for directional light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Directional         

    CGPSHader = COMBINER1
        
    CGVProgram = CGVProgBump_DiffSpec_Gloss_EnvLight
    CGVPParam ( Name = EnvColors EnvColor )
    CGVPParam ( Name = TexMatrix TranspObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )
    Layer '3'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '4'
    (
      Map = $Gloss
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )                 
  )
)

//======================================================================

// Specular lighting pass for projected light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Projected

    CGPSHader = COMBINER3
        
    CGVProgram = CGVProgBump_DiffSpecPass_ProjAtten_Gloss_EnvLight_VS20
    CGVPParam ( Name = EnvColors EnvColor )
    CGVPParam ( Name = TexMatrix TranspObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)
    CGVPParam ( Name = LightMatrix TranspLightMatrix )

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )
    Layer '3'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '4'
    (
      Map = $FromLight
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '5'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
    )                 
    Layer '6'
    (
      Map = $Gloss
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )                 
  )
)

//======================================================================

// Specular lighting pass for point light source
Light 'Specular'
(         
  ShadeLayer
  (     
    LightType = Point

    CGPSHader = COMBINER5
        
    CGVProgram = CGVProgBump_DiffSpecPass_Atten_Gloss_EnvLight
    CGVPParam ( Name = EnvColors EnvColor )
    CGVPParam ( Name = TexMatrix TranspObjMatrix )
    CGVPParam ( Name = LightPos Comp 'OSLightPos[0]' Comp 'OSLightPos[1]' Comp 'OSLightPos[2]' Comp=1)
    CGVPParam ( Name = CameraPos Comp 'OSCameraPos pos 0' Comp 'OSCameraPos pos 1' Comp 'OSCameraPos pos 2')
    CGVPParam ( Name = AttenInfo Comp 'LightIntens' Comp 'InvLightIntens' Comp=0.5)

    Layer '0'
    (
      Map = $Diffuse
      TexColorOp = NoSet
    )                 
    Layer '1'
    (
      Map = $Bump
      TexColorOp = NoSet
    )                 
    Layer '2'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )
    Layer '3'
    (
      Map = $NormalizationCubeMap
      TexType = Cubemap 
      TexColorOp = NoSet
    )                 
    Layer '4'
    (
      Map = $Attenuation2D
      ClampTexCoords
      TexColorOp = NoSet
    )                 
    Layer '5'
    (
      Map = $Gloss
      TexColorOp = NoSet
      SecondPassRendState
      {
        Blend 'ONE ONE'
        DepthFunc = Equal
        DepthWrite = 0
      }
    )                 
  )
)
