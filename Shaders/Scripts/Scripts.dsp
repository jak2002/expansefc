# Microsoft Developer Studio Project File - Name="Scripts" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 60000
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=Scripts - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Scripts.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Scripts.mak" CFG="Scripts - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Scripts - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "Scripts - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/MASTERCD/SHADERS/Scripts", NHGBAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Scripts - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "Scripts - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "Scripts - Win32 Release"
# Name "Scripts - Win32 Debug"
# Begin Group "CryShaders"

# PROP Default_Filter ""
# Begin Group "Objects"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\CryShaders\Objects\ObjClasses.csl
# End Source File
# End Group
# Begin Group "Procedures"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\CryShaders\Procedures\liquid_procedures.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\Procedures\liquid_procedures_bump.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\Procedures\Procedures4.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\Procedures\spark_procedures.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\Procedures\special_procedures.csl
# End Source File
# End Group
# Begin Group "System"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\CryShaders\System\LightMaterials.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\System\LightStyles.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\System\Orients.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\System\PhysMaterials.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\System\SunFlares.csl
# End Source File
# End Group
# Begin Source File

SOURCE=.\CryShaders\CryInd.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\GlLight.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\Lights.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\Marco.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\Noise.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\plants.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\playerHero.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\playerP1.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\Sky.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\Templates.csl
# End Source File
# Begin Source File

SOURCE=.\CryShaders\Weapons.csl
# End Source File
# End Group
# Begin Group "Basic"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\basic\flag_bump.csl
# End Source File
# Begin Source File

SOURCE=.\basic\hellfire.csl
# End Source File
# Begin Source File

SOURCE=.\basic\hotmetal_bump.csl
# End Source File
# Begin Source File

SOURCE=.\basic\lines.csl
# End Source File
# Begin Source File

SOURCE=.\basic\liquid_fog.csl
# End Source File
# Begin Source File

SOURCE=.\basic\liquids_basic.csl
# End Source File
# Begin Source File

SOURCE=.\basic\liquids_bump.csl
# End Source File
# Begin Source File

SOURCE=.\basic\morphing_bump.csl
# End Source File
# Begin Source File

SOURCE=.\basic\player_spawn.csl
# End Source File
# Begin Source File

SOURCE=.\basic\polygons.csl
# End Source File
# Begin Source File

SOURCE=.\basic\teleporter.csl
# End Source File
# Begin Source File

SOURCE=.\basic\weapon_actions.csl
# End Source File
# End Group
# Begin Group "Honya"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Honya\Bump.csl
# End Source File
# Begin Source File

SOURCE=.\Honya\Fog.csl
# End Source File
# Begin Source File

SOURCE=.\Honya\Portal.csl
# End Source File
# Begin Source File

SOURCE=.\Honya\q3.csl
# End Source File
# Begin Source File

SOURCE=.\Honya\Ter.csl
# End Source File
# End Group
# Begin Group "Landa"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Landa\common.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\comp.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\Env.csl
# End Source File
# Begin Source File

SOURCE=".\Landa\lava&acid.csl"
# End Source File
# Begin Source File

SOURCE=.\Landa\MyFlare.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\myFog.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\Mylight.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\Mymir.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\mymorph.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\mysnow.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\optic.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\PARTgrav.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\snow.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\tesla.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\Tree.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\trick.csl
# End Source File
# Begin Source File

SOURCE=.\Landa\wreck.csl
# End Source File
# End Group
# Begin Group "Prefabs"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Prefabs\Prefab.csl
# End Source File
# End Group
# Begin Group "Renderer"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Renderer\crosshairs.csl
# End Source File
# Begin Source File

SOURCE=.\Renderer\simpleitems.csl
# End Source File
# End Group
# Begin Group "RudyBear"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\RudyBear\font.csl
# End Source File
# Begin Source File

SOURCE=.\RudyBear\Menus.csl
# End Source File
# Begin Source File

SOURCE=.\RudyBear\Player.csl
# End Source File
# End Group
# Begin Group "Vicont"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Vicont\EnvSound.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Fire.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Flag.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Fog.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Lamp.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Lamp_Bump.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Lena.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Lena_Bump.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Lighting.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Morphanim.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Nature.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Particle.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Portal.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Scroll.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Sky.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Technical.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Transhole.csl
# End Source File
# Begin Source File

SOURCE=.\Vicont\Translut.csl
# End Source File
# End Group
# Begin Group "Yez"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\yez\yez.csl
# End Source File
# End Group
# Begin Group "Zona"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Zona\bbald.csl
# End Source File
# End Group
# Begin Source File

SOURCE=.\BlendAnims.csl
# End Source File
# Begin Source File

SOURCE=.\BlendPolys.csl
# End Source File
# Begin Source File

SOURCE=.\Blood.csl
# End Source File
# Begin Source File

SOURCE=.\decor.csl
# End Source File
# Begin Source File

SOURCE=.\EQU.csl
# End Source File
# Begin Source File

SOURCE=.\MARK.csl
# End Source File
# Begin Source File

SOURCE=.\priv.csl
# End Source File
# Begin Source File

SOURCE=.\Sprites.csl
# End Source File
# End Target
# End Project
