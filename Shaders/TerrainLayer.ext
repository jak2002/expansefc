////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Shader extension
//  Copyright (C), Crytek Studios, 2001-2004.
// -------------------------------------------------------------------------
//  File name:   TerrainLayer.ext
//  Version:     v1.00
//  Created:     16/2/2004 by Andrey Honich.
//  Compilers:   
//  Description: TerrainLayer shader extension used by the editor
//               for automatic shader generation (based on "TerrainLayer" shader template)
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////

Version (1.00)

Property
{
  Name = %DIFFUSE
  Mask = 0x1
  Property    (Diffuse lighting)
  Description (Use diffuse lighting)
}
Property
{
  Name = %SPECULAR
  Mask = 0x2
  Property    (Specular lighting)
  Description (Use specular lighting)
}
Property
{
  Name = %BUMP_MAP
  Mask = 0x4
  Property    (Bump map)
  Description (Use bump-map texture)
}
Property
{
  Name = %ALPHAGLOW
  Mask = 0x8
  Property    (Glow in Dif. alpha)
  Description (Use alpha channel of diffuse texture for glow)
}
Property
{
  Name = %DIFFUSEALPHA
  Mask = 0x10
  Property    (Diffuse alpha out)
  Description (Output alpha channel of diffuse texture for transparency)
}
Property
{
  Name = %BUMP_NORMALIZE
  Mask = 0x20
  Property    (Normalize Bump)
  Description (Use per-pixel normalization of bump normals)
}
Property
{
  Name = %ENVCMAMB
  Mask = 0x40
  Property    (Ambient EnvCM)
  Description (Use Env. CM reflections without fresnel)
}
Property
{
  Name = %ENVCMSPEC
  Mask = 0x80
  Property    (Specular EnvCM)
  Description (Use per-pixel Env. CM reflections with fresnel)
}
Property
{
  Name = %ENVCM_MASKED
  Mask = 0x100
  Property    (Masked EnvCMAmb)
  Description (Use EnvCM reflections masked by alpha channel from gloss alpha)
}
Property
{
  Name = %GLOSS_MAP
  Mask = 0x200
  Property    (Gloss map)
  Description (Use gloss map as separate texture)
}
Property
{
  Name = %GLOSS_DIFFUSEALPHA
  Mask = 0x400
  Property    (Gloss map DifAlpha)
  Description (Use gloss map as alpha channel in diffuse texture)
}
Property
{
  Name = %RTCUBEMAP
  Mask = 0x800
  Property    (RealTime cube-map)
  Description (Use real-time Cube-Map updating)
}
Property
{
  Name = %OFFSETBUMPMAPPING
  Mask = 0x1000
  Property    (Offset bump-mapping)
  Description (Use offset-bump-mapping (requires height-map in alpha-channel of bump texture))
}
Property
{
  Name = %SPECULAR_PERPIXEL
  Mask = 0x2000
  Property    (PerPixel Specular)
  Description (Calculate specular completelly on per-pixel basis)
}
Property
{
  Name = %SPECULARPOW_GLOSSALPHA
  Mask = 0x4000
  Property    (PerPixel Spec. Shinines)
  Description (Use specular shininess coef. as alpha channel of gloss texture)
}
Property
{
  Name = %ALPHA_BLEND
  Mask = 0x8000
  Property    (Alpha Blend)
  Description (Use alpha-blending for diffuse terrain pass)
}
